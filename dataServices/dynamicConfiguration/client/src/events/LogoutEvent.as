/* Copyright 2008 Mike Slinn (mslinn@mslinn.com)
 * 
 * Mike Slinn licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * $Id: LogoutEvent.as 658 2009-07-03 22:55:54Z Mike Slinn $ */

package events {
	import flash.events.Event;
	
	public class LogoutEvent extends Event {
		public static const LOGOUT:String = "com.mslinn.service.Logout";
		
		public function LogoutEvent() {
			super(LOGOUT, false, true);
		}
        
        override public function clone():Event {
            return new LogoutEvent();
        }
	}
}