/* Copyright 2007-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Significantly altered by Mike Slinn (mslinn@mslinn.com) */

 
 package services {
    import events.LoginEvent;
    import events.LogoutEvent;
    import flash.events.TimerEvent;
    import flash.utils.Timer;
    import model.Model;
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.logging.ILogger;
    import mx.logging.Log;
    import mx.managers.CursorManager;
	import mx.messaging.messages.ErrorMessage;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.remoting.mxml.RemoteObject;
	import mx.rpc.remoting.Operation;

	public class AmfServices {
        public var service:RemoteObject;
        private var _endpoint:String;
        private var operations:Object = new Object();
        private var rsFactory:AMFFactory = AMFFactory.getInstance();

        /** For login() context */
        private var serverName:String;
        private var _userId:String;
        private var _password:String;

        /** For login() context */
        private var serverUrl:String;
        
        private var serviceDestination:String = "sampleService";
        
        /** Events are dispatched from here */
        private var eventSyncPoint:Object;
        private static const logger:ILogger = Log.getLogger("RPC");


        
	    public function AmfServices(eventSyncPoint:Object) {
	    	this.eventSyncPoint = eventSyncPoint;
	    }
	    
        public function get endpoint():String { return _endpoint; }

        /** Creates new RemoteObject every time the endpoint changes because an Operation's endpoint cannot be modified */        
	    public function set endpoint(newEndpoint:String):void {
	    	_endpoint = newEndpoint;
            operations = { // RPC function name:Operation name/value pairs
                // RPCs not associated with specific menu items:
                login :rsFactory.createOperation("login",  loginSuccess),
                logout:rsFactory.createOperation("logout", logoutSuccess)
            };
	    	service = rsFactory.createRemoteService(serviceDestination, operations, _endpoint);
	    }

        public function login(app:Model, userId:String, password:String, serverUrl:String):void {
            app.endpoint = serverUrl;
            if (service!=null) {
                rsFactory.stopWatch.startTimer();
                this.serverName = serverName;
                this.serverUrl = serverUrl;
                _userId = userId;
                _password = password;
                service.login(userId, password);
            }
        }

        public function loginSuccess(event:ResultEvent):void {
            CursorManager.removeBusyCursor();
            var kSpaceNames:ArrayCollection = event.result.kSpaceNames==null ? new ArrayCollection() : event.result.kSpaceNames;
            var signonEvent:LoginEvent = new LoginEvent(serverUrl, serverName, _userId, _password, new Date());
            eventSyncPoint.dispatchEvent(signonEvent);
            rsFactory.stopWatch.reportElapsedTime("AmfServices.login()");
        }
        
        
        public function logout():void {
            if (service!=null) {
                rsFactory.stopWatch.startTimer();
                eventSyncPoint.dispatchEvent(new LogoutEvent());
                service.logout();
            }
        }

        public function logoutSuccess(event:ResultEvent):void {
            CursorManager.removeBusyCursor();
            rsFactory.stopWatch.reportElapsedTime("AmfServices.logout()");
        }
    }
}