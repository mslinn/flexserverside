/* Copyright 2008 Mike Slinn (mslinn@mslinn.com)
 * 
 * Mike Slinn licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * $Id: Model.as 658 2009-07-03 22:55:54Z Mike Slinn $ */

package model {
	import services.AmfServices;
	
	[Bindable] public class Model {
		private static var instance:Model = new Model();
		private var _endpoint:String = "";
        public var amfServices:AmfServices;
        public var href:String = "";
		
		public function Model() {
			amfServices = new AmfServices(this);
		}

		public function get endpoint():String { return _endpoint; }
        
        public function set endpoint(endPt:String):void {
        	_endpoint = endPt;
            amfServices.endpoint = _endpoint;
        }
        
        public static function getInstance():Model { return instance; }
	}
}