import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
		import flex.messaging.FlexContext;
		import flex.messaging.FlexSession;
		import flex.messaging.HttpFlexSession;
		import flex.messaging.client.FlexClient;
import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;

		/* The <tt>FlexContext</tt> is a utility class that exposes the current execution context.
		 * It provides access to <tt>FlexSession</tt> and <tt>FlexClient</tt> instances associated
		 * with the current message being processed, as well as global context via the <tt>MessageBroker</tt>,
		 * <tt>ServletContext</tt> and <tt>ServletConfig</tt> for the application.
		 * Any, or all, of the properties exposed by this class may be <code>null</code> depending upon
		 * the current execution context so test for that before attempting to interact with them. */
		/* The HttpServletResponse for the current request if the request is via HTTP.
	     * Returns null if the client is using a non-HTTP channel. */
		HttpServletRequest httpServletRequest = FlexContext.getHttpRequest();
		/* The ServletConfig for the current request, uses the last known ServletConfig
	     * when the request is not via HTTP. */

		ServletConfig servletConfig = FlexContext.getServletConfig();
		 /* The ServletContext for the current web application. */

		ServletContext servletContext = FlexContext.getServletContext();
		/* The FlexClient for the current request. */
		FlexClient flexClient = FlexContext.getFlexClient();

		/* The FlexSession for the current request. */
		FlexSession flexSession = FlexContext.getFlexSession();

		/* The HttpServletRequest for the current request if it is transporting a tunneled protocol.
	     * Returns null if the current request protocol it not tunneled. */
		HttpServletRequest httpServletRequest2 = FlexContext.getTunnelHttpRequest();

		/* The HttpServletResponse for the current request if the request is via HTTP.
        * Returns null if the using an non-HTTP channel. */
		HttpServletResponse httpServletResponse = FlexContext.getHttpResponse();

		/* Indicates whether the current message being processed came from a server peer
	     * in a cluster. */
	    boolean peeredMsg = FlexContext.isMessageFromPeer();

	    /* Returns the principal associated with the session or client depending on whether
	     * perClientauthentication is being used.  If the client has not
	     * authenticated the principal will be null. */
	    Principal principal = FlexContext.getUserPrincipal();

	    /* Set the Principal on either the current FlexClient or FlexSession depending upon whether
	     * perClientAuthentication is in use. */
	    FlexContext.setUserPrincipal(null);


		/* Returns the unique Id for the FlexClient. */
		String clientId = flexClient.getId();
		logger.info("Flex client ID=" + clientId);

		/* Returns a snapshot of the FlexSessions associated with the FlexClient when this method is invoked.
		 * This list is not guaranteed to remain consistent with the actual list of active FlexSessions associated with the FlexClient over time. */
		List flexClientSessions = flexClient.getFlexSessions();
		logger.info(flexClientSessions.size() + " flex client sessions");

		/* Returns true if the FlexClient is valid; false if it has been invalidated. */
		boolean validClient = flexClient.isValid();
		logger.info("Flex client valid=" + validClient);

		/* Returns a snapshot of the FlexSessions associated with the FlexClient when this
		 * method is invoked. This list is not guaranteed to remain consistent with the actual list of active FlexSessions associated with the FlexClient over time.  */
		int flexClientSessionCount = flexClient.getSessionCount();
		logger.info("Flex client session count=" + flexClientSessionCount);

		/* The number of subscriptions associated with this FlexClient. */
		int flexSubscriptions = flexClient.getSubscriptionCount();
		logger.info("Flex client subscription count=" + flexSubscriptions);

		/* Returns a snapshot of the MessageClients (subscriptions) associated with the
		 * FlexClient when this method is invoked. This list is not guaranteed to remain
		 * consistent with the actual list of active MessageClients associated with the FlexClient over time. */
		List messageClients = flexClient.getMessageClients();
		logger.info("Flex client messageClients count=" + messageClients.size());

		/* Binds an attribute value for the FlexClient under the specified name. */
		flexClient.setAttribute("name", "value");

		/* Returns the attribute bound to the specified name for the FlexClient, or null if
		 * no attribute is bound under the name. */
		Object atttributeValue = flexClient.getAttribute("name");
		logger.info("Flex client attribute value=" + atttributeValue);

		/* Returns a snapshot of the names of all attributes bound to the FlexClient.  */
		Enumeration clientAttributes = flexClient.getAttributeNames();

		/* The 'last use' timestamp for the FlexClient, which may be the current system time
		 * if the FlexClient has been idle but an open connection from the client to the
		 * server exists. */
		long lastUseTimestamp = flexClient.getLastUse();
		logger.info("Flex client last used=" + new java.util.Date(lastUseTimestamp));

		/* Invalidates the FlexClient. */
		//flexClient.invalidate();

		/* Returns true if the FlexClient is valid; false if it has been invalidated.  */
		boolean flexClientValid = flexClient.isValid();
		logger.info("Flex client is valid=" + flexClientValid);

	    /* Creates or retrieves a FlexSession for the current Http request. The HttpFlexSession wraps the underlying J2EE HttpSession.  */
	    HttpFlexSession httpFlexSession = HttpFlexSession.getFlexSession(httpServletRequest);

	    /* Returns the Id for the session (usually stored in a cookie). */
	    String sessionId = httpFlexSession.getId();
		logger.info("Flex session ID=" + sessionId);
