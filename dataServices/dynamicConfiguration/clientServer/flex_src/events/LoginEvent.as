/* Copyright 2008 Mike Slinn (mslinn@mslinn.com)
 *
 * Mike Slinn licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * $Id: LoginEvent.as 972 2009-08-16 05:36:23Z Mike Slinn $ */
package events {
  import flash.events.Event;


  public class LoginEvent extends Event {
    public static const LOGIN:String = "com.mslinn.service.Login";
    public var userId:String;
    public var password:String;
    public var timestamp:Date;

    public function LoginEvent(userId:String, password:String, timestamp:Date) {
      super(LOGIN, false, true);
      this.userId = userId;
      this.password = password;
      this.timestamp = timestamp;
    }

    override public function clone():Event {
      return new LoginEvent(userId, password, timestamp);
    }
  }
}