package com.adobe.adapter;

import flex.messaging.messages.CommandMessage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import flex.messaging.MessageDestination;
import flex.messaging.messages.AsyncMessage;
import flex.messaging.messages.Message;
import flex.messaging.services.MessageService;
import flex.messaging.services.ServiceAdapter;

public class CustomMessagingAdapter extends ServiceAdapter {
    private ArrayList<String> history = new ArrayList<String>();
    private Set<Object> subscriberIds = new HashSet<Object>();
    private Object operation;
    private AsyncMessage asyncMessage;
    private MessageService msgService;
    
	@Override
	public Object invoke(Message message) {
        msgService = (MessageService)getDestination().getService();
		asyncMessage = (AsyncMessage)message;
		operation = asyncMessage.getHeader("operation");
		if (operation!=null && operation.toString().compareTo("getHistory")==0) {
		    asyncMessage.setBody(formatHistory());
		    subscriberIds.clear();
            subscriberIds.add(message.getHeader("consumerId")); 
            msgService.pushMessageToClients((MessageDestination)getDestination(), subscriberIds, asyncMessage, false);
		} else { // broadcast message
		    String msgText = "From " + message.getClientId() + ": " + message.getBody().toString() + "\n";
		    history.add(msgText);
    		asyncMessage.setBody(msgText);
    		msgService.pushMessageToClients(asyncMessage, true);
		}
		return "An Object was returned in the invoke() acknowledge message";
	}
	
	@Override
	public Object manage(CommandMessage commandMessage) {
		Object object = super.manage(commandMessage);
		return object;
	}
	
	 @Override
	 public boolean handlesSubscriptions() {
	    return true;
	 }
	
	private String formatHistory() {
	    StringBuilder string = new StringBuilder();
	    String delim = "";
	    for (String item : history) {
	        string.append(item).append(delim);
	        delim = "\n";
	    }
	    return string.toString();
	}
}
