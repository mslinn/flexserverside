Run this project from http://localhost:8080/DatabasePush/DatabasePush.html?debug=true

In general, push database updates to clients using LCDS as follows:

1) Create  a class implementing AbstractBootstrapService

2) Register this class in services.xml

    <services>
        <service-include file-path="remoting-config.xml" />
        <service-include file-path="proxy-config.xml" />
        <service-include file-path="messaging-config.xml" />
        <service-include file-path="data-management-config.xml" />
        <service class="fiber.data.services.ModelDeploymentService" id="model-deploy-service" />
        <service class="com.test.BootstrapService" id="bootstrapService">
    </service> 

3) Add the following code in the start method of the bootstrap service

    public void start() {
        DataServiceTransaction dtx = DataServiceTransaction.begin(true);
        Item item = new Item();
        item.setName("Test created in bootstrap at: " + System.nanoTime());           
        new ItemDAO().create(item);
        dtx.refreshFill("Test", null, null);
        dtx.commit(); 
    }
	  
4) Add a file named context.xml under META-INF folder. It should have the following content:

<Context reloadable="true" privileged="true" antiResourceLocking="false" antiJARLocking="false">
    <Transaction factory="org.objectweb.jotm.UserTransactionFactory" jotm.timeout="60"/>
</Context>


Notes
=====

1) src/connection.properties defines a database called test, with userid root and password asdf.

2) Hibernate is tightly bundled with LCDS 3; hibernate3.jar must be present.  This is unfortunate.

3) If you do not want to use the combination trigger+db+bootstrap service you can push your data from the trigger into a JMS queue in almost real time. This should be possible in MySQL, Oracle and IBM db/2.
