package com.mslinn.dbUpdate {

    [Managed]
    [RemoteClass(alias="com.mslinn.dbUpdate.Item")]
    public class Item {
        public var id:int;
        public var name:String;
    }
}