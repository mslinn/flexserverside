package com.mslinn.dbUpdate;

public class Item {
    public int id;
    public String name;
    
    public Item() {}

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }
}
