package com.mslinn.dbUpdate;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;


public class ItemDAO {

    public List getProducts() {
        List list = new ArrayList();
        Connection connection = null;
        try {            
            connection = DbHelper.getConnection();
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM test ORDER BY name");
            while (rs.next()) {
                Item item = new Item();
                item.setId(rs.getInt("id"));
                item.setName(rs.getString("name"));
                list.add(item);
            }            
        } catch (Exception e) {            
            throw new RuntimeException(e);
        } finally {
            DbHelper.close(connection);
        }
        return list;

    }

    public Item getProduct(int productId) {
        Item product = new Item();        
        Connection connection = null;
        try {
            connection = DbHelper.getConnection();
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM test WHERE id=" + productId);
            if (rs.next()) {
                product = new Item();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
            }
            s.close();            
        } catch (Exception e) {            
            throw new RuntimeException(e);
        } finally {
            DbHelper.close(connection);
        }
        return product;
    }

    public Item create(Item product) {       
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DbHelper.getConnection();
            ps = connection.prepareStatement("INSERT INTO test (name) VALUES (?)");
            ps.setString(1, product.getName());
            ps.executeUpdate();
//            ResultSet rs = ps.getGeneratedKeys();
//            if (rs.next())
//                product.setId(rs.getInt(1));
            ps.close();
            connection.commit();            
        } catch (Exception e) {
        	e.printStackTrace();
            throw new RuntimeException(e);
        } finally {            
            DbHelper.close(connection);
        }
        return product;
    }

    public boolean update(Item product) {
        Connection connection = null;
        try {
            connection = DbHelper.getConnection();
            PreparedStatement ps = connection.prepareStatement("UPDATE test SET name=? WHERE id=?");
            ps.setString(1, product.getName());
            ps.setInt(2, product.getId());
            int count = ps.executeUpdate();
            ps.close();
            connection.commit();            
            return (count == 1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            DbHelper.close(connection);    
        }
    }

    public boolean delete(Item product){        
        Connection connection = null;        
        try {
            connection = DbHelper.getConnection();
            Statement stmt = connection. createStatement();
            int count = stmt.executeUpdate("DELETE FROM test WHERE id='"+product.getId()+"'");
            stmt.close();
            connection.commit();            
            return (count == 1);
        } catch (Exception e) {            
            throw new RuntimeException(e);
        } finally {
            DbHelper.close(connection);
        }
    }
}
