package com.mslinn.dbUpdate;

import java.sql.*;
import java.util.Properties;

public class DbHelper {
    private static DbHelper instance;
    private static Properties properties;
    private static final String DB = "db";
    private static final String USER = "user";
    private static final String PASSWD = "passwd";
    private static final String DRIVER = "driver";
    
    private DbHelper() throws Exception{
        String driver = null;
        try {
            properties = new Properties();
            properties.load(DbHelper.class.getClassLoader().getResourceAsStream("connection.properties"));
            Class.forName(properties.getProperty(DRIVER));            
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR: Error loading JDBC driver. Class " + driver + "not found.");
        }
    }    
    
    public static Connection getConnection() throws Exception {
        Connection connection = null;
        if (instance == null) {
            instance = new DbHelper();
        } try {
            connection = DriverManager.getConnection(properties.getProperty(DB), properties.getProperty(USER), properties.getProperty(PASSWD));
            connection.setAutoCommit(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(Connection connection) {
        try {
            if (connection != null) 
                connection.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void close(PreparedStatement ps) {
        try {
            if (ps != null) 
                ps.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
}
