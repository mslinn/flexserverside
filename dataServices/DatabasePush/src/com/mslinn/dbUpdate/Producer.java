package com.mslinn.dbUpdate;

import java.util.Collection;
import flex.data.DataDestination;
import flex.data.DataServiceTransaction;
import flex.data.messages.DataMessage;
import flex.data.messages.InternalMessage;
import flex.messaging.io.BeanProxy;
import flex.messaging.io.amf.ASObject;

public class Producer {
    
    public void createNewItem() {
        try {
            DataDestination dt = DataDestination.getDataDestination("Test");     
            DataMessage fillMessage = InternalMessage.createMessage();
            fillMessage.setOperation(DataMessage.CREATE_OPERATION);                
            ASObject test = new ASObject();
            test.put("name", "Test created at:" + System.nanoTime());
            test.setType("com.mslinn.dbUpdate.Item");                        
            BeanProxy proxy = new BeanProxy(test);
            fillMessage.setBody(proxy);
                            
            DataServiceTransaction tx = DataServiceTransaction.begin(false);  
            Collection<Object> result = (Collection<Object>)dt.getAdapter().invoke(fillMessage);  
            tx.refreshFill("Test", null, null);
            tx.commit(); 
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }    
    
    public void createNewItem2() {
        try {
            DataServiceTransaction dtx = DataServiceTransaction.getCurrentDataServiceTransaction();
            Item item = new Item();
            item.setName("Test created at:" + System.nanoTime());            
            new ItemDAO().create(item);
            dtx.refreshFill("Test", null, null);
            dtx.commit();               
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
