package com.mslinn.dbUpdate;

import flex.data.DataServiceTransaction;
import flex.messaging.config.ConfigMap;
import flex.messaging.services.AbstractBootstrapService;

public class BootstrapService extends AbstractBootstrapService {

	@Override
	public void initialize(String arg0, ConfigMap arg1) {
	}

	@Override
	public void start() {
		DataServiceTransaction dtx = DataServiceTransaction.begin(true); 
        Item item = new Item();
        item.setName("Test created in bootstrap at:"+System.nanoTime());            
        new ItemDAO().create(item);
        dtx.refreshFill("Test", null,null);
        dtx.commit();  

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
	}
}