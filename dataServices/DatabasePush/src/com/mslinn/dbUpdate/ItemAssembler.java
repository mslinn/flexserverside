package com.mslinn.dbUpdate;

import java.util.Collection;
import java.util.List;
import java.util.Collection;
import java.util.Map;

import flex.data.DataSyncException;
import flex.data.assemblers.AbstractAssembler;

public class ItemAssembler extends AbstractAssembler {

    public Collection fill(List fillArgs) {        
        ItemDAO service = new ItemDAO();
        return service.getProducts();
    }

    public Object getItem(Map identity) {
        ItemDAO service = new ItemDAO();
        return service.getProduct(((Integer) identity.get("id")).intValue());
    }

    public void createItem(Object item) {
        ItemDAO service = new ItemDAO();
        service.create((Item) item);
        new Producer().createNewItem2();
    }

    public void updateItem(Object newVersion, Object prevVersion, List changes) {        
        ItemDAO service = new ItemDAO();
        boolean success = service.update((Item) newVersion);
        if (!success) {
            int productId = ((Item) newVersion).getId();
            throw new DataSyncException(service.getProduct(productId), changes);
        }
    }

    public void deleteItem(Object item) {        
        ItemDAO service = new ItemDAO();
        boolean success = service.delete((Item) item);
        if (!success) {
            int productId = ((Item) item).getId();
            throw new DataSyncException(service.getProduct(productId), null);
        }
    }
}