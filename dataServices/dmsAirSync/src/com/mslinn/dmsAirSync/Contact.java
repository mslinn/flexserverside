package com.mslinn.dmsAirSync;
	import javax.persistence.Column;
	import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
	import javax.persistence.Id;
import javax.persistence.NamedQueries;
	import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="contact")
@NamedQueries({
	@NamedQuery(name="all", query="from Contact order by last_name, first_name"),
	@NamedQuery(name="byName", query="from Contact where lower(first_name) like :name or lower(last_name) like :name order by last_name, first_name")
})
public class Contact {
	private int contactId;
	private String email;
	private String firstName;
	private String lastName;
	private String phone;
	private String title;

	@Id
	@GeneratedValue
	@Column(name="contact_id")
	public int getContactId() { return contactId; }

	public String getEmail() { return email; }

	@Column(name="first_name")
	public String getFirstName() { return firstName; }

	@Column(name="last_name")
	public String getLastName() { return lastName; }

	public String getPhone() { return phone; }

	public String getTitle() { return title; }

	public void setContactId(int contactId) { this.contactId = contactId; }

	public void setEmail(String email) { this.email = email; }

	public void setFirstName(String firstName) { this.firstName = firstName; }

	public void setLastName(String lastName) { this.lastName = lastName; }

	public void setPhone(String phone) { this.phone = phone; }

	public void setTitle(String title) { this.title = title; }
}
