package com.mslinn.dmsAirSync;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;
import com.mslinn.HibernateUtil;


public class ContactTest {
	@Test public void testFindAll() {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.getNamedQuery("all");
		for (Object object : query.list()) {
			Contact contact = (Contact) object;
			System.out.println(contact.getFirstName() + " " + contact.getLastName());
		}
		transaction.commit();
	}
	
	@SuppressWarnings("unchecked")
	@Test public void testFindByName() {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.getNamedQuery("byName");
        List<Contact> list = query.setString("name", "slinn").list(); // pass in a lower case name
		for (Object object : list) {
			Contact contact = (Contact)object;
			System.out.println(contact.getFirstName() + " " + contact.getLastName());
			return;
		}
		fail("Did not find desired record");
		transaction.commit();
	}
	
	@Test public void testRetrieve() {
		Session session = HibernateUtil.getSession();
	    Transaction transaction = session.beginTransaction();
	    try {
	        Contact contact = (Contact)session.get(Contact.class, 9);
	        assertEquals(contact.getEmail(), "mslinn@mslinn.com");
	        
	        contact = new Contact();
	    	contact.setEmail("fflintstone@bedrock.com");
	    	contact.setFirstName("Fred");
	    	contact.setLastName("Flintstone");
	    	contact.setPhone("123-456-7890");
	    	contact.setTitle("Quarry worker");
	    	session.saveOrUpdate(contact);
	        assertEquals(contact.getContactId(), 10);
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	    } finally {
	        transaction.commit();
	    }
	}
}
