@REM this script copies LCDS proprietary JARs and SWCs to the demo project

@REM Try to find where LCDS was installed:
if exist "c:\lcds\resources" set LCDS=C:\lcds\resources
if exist "d:\lcds\resources" set LCDS=D:\lcds\resources
if exist "e:\lcds\resources" set LCDS=E:\lcds\resources
if exist "c:\program files\lcds\resources" set LCDS=C:\Program Files\lcds\resources
if exist "D:\program files\lcds\resources" set LCDS=D:\Program Files\lcds\resources
if exist "E:\program files\lcds\resources" set LCDS=E:\Program Files\lcds\resources
if exist "c:\program files\adobe\lcds\resources" set LCDS=C:\Program Files\Adobe\lcds\resources
if exist "D:\program files\adobe\lcds\resources" set LCDS=D:\Program Files\Adobe\lcds\resources
if exist "E:\program files\adobe\lcds\resources" set LCDS=E:\Program Files\Adobe\lcds\resources

copy "%LCDS%\frameworks\libs\fds.swc"             flex_libs
copy "%LCDS%\frameworks\locale\en_US\fds_rb.swc"  flex_libs
copy "%LCDS%\frameworks\libs\air\airfds.swc"      flex_libs
copy "%LCDS%\frameworks\libs\player\playerfds.swc" flex_libs

copy "%LCDS%\lib\flex-messaging-common.jar"    WebContent\WEB-INF\lib
copy "%LCDS%\lib\flex-messaging-core.jar"      WebContent\WEB-INF\lib
copy "%LCDS%\lib\flex-messaging-data.jar"      WebContent\WEB-INF\lib
copy "%LCDS%\lib\flex-messaging-data-req.jar"  WebContent\WEB-INF\lib
copy "%LCDS%\lib\flex-messaging-opt.jar"       WebContent\WEB-INF\lib
copy "%LCDS%\lib\flex-messaging-proxy.jar"     WebContent\WEB-INF\lib
copy "%LCDS%\lib\flex-messaging-remoting.jar"  WebContent\WEB-INF\lib

del WebContent\WEB-INF\lib\*.removed
