@rem Replaces a proprietary file with an empty file of the same name, with filetype 'removed'

@echo .> %1.removed
@del %1
