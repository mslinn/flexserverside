/* Copyright 2008 Mike Slinn (mslinn@mslinn.com)
 * Mike Slinn licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * $Id: OnscreenTraceTarget.as 1754 2010-02-17 15:07:50Z mslinn $ */

package util {
    import mx.controls.TextArea;
    import mx.core.Application;
    import mx.logging.LogEvent;
    import mx.logging.targets.TraceTarget;

    public class OnscreenTraceTarget extends TraceTarget {
        public function OnscreenTraceTarget() {
            super();
        }
        
        override public function logEvent(event:LogEvent):void {
            debugMsg(event.message);
        }
        
        public function debugMsg(msg:String): void {
        	if (Application.application && Application.application.hasOwnProperty("msgArea")) {
	            var msgArea:TextArea = Application.application.msgArea;
	            msgArea.text += msg + "\n";
	        }
            trace(msg);
        }
    }
}