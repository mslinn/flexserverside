package com.mslinn.AScriptMsg;

import flex.messaging.MessageBroker;
import flex.messaging.messages.AsyncMessage;
import flex.messaging.util.UUIDUtils;

/** Push message to specified destination using the ActionScript adapter. */
public class AScriptMsgGenerator {
	private static MessageBroker mbroker = MessageBroker.getMessageBroker(null);

	public static void pushMessage(String destination, String msg) {
		AsyncMessage message = new AsyncMessage();
		message.setDestination(destination);
		message.setMessageId(UUIDUtils.createUUID(false));
		message.setTimestamp(System.currentTimeMillis());
		message.setBody(msg);
		mbroker.routeMessageToService(message, null);
	}
}
