package com.mslinn.lazy;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import com.mslinn.HibernateUtil;
import com.mslinn.lazy1.*;

public class SchoolClassTest {
	private static Session session;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		session = HibernateUtil.getSession();
	}

	@Test public void testFindAll() {
		Transaction transaction = session.beginTransaction();
		Query query = session.getNamedQuery("schoolClasses");
		for (Object object : query.list()) {
			SchoolClass schoolClass = (SchoolClass) object;
			System.out.println(schoolClass.getName());
		}
		transaction.commit();
	}
	
	@After
	public void tearDown() throws Exception {
		session.close();
	}

}
