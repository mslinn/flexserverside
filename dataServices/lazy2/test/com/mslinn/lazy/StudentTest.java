package com.mslinn.lazy;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import com.mslinn.HibernateUtil;
import com.mslinn.lazy1.*;

public class StudentTest {
	private static Session session;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		session = HibernateUtil.getSession();
	}

	@Test public void testFindAll() {
		Transaction transaction = session.beginTransaction();
		Query query = session.getNamedQuery("all.students");
		for (Object object : query.list()) {
			Student student = (Student) object;
			System.out.println(student.getFirstName() + " " + student.getLastName());
		}
		transaction.commit();
	}
	
	@After
	public void tearDown() throws Exception { }
}
