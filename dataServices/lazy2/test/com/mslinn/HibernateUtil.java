package com.mslinn;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.impl.SessionFactoryImpl;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import com.mslinn.lazy1.*;

/** @see http://jpa.ezhibernate.com/Javacode/learn.html?tutorial=05samplehibernateutilclassexample */
public class HibernateUtil {
	private static SessionFactory factory;

	
	public static Configuration getInitializedConfiguration() {
		AnnotationConfiguration config = new AnnotationConfiguration();
		/* add JPA annotated classes here */
		config.addAnnotatedClass(SchoolClass.class);
		config.addAnnotatedClass(Student.class);
		config.configure();
		return config;
	}

	public static Session getSession() {
		if (factory == null) {
			Configuration config = HibernateUtil.getInitializedConfiguration();
			factory = config.buildSessionFactory();
		}
		return factory.getCurrentSession();
	}

	/** For SquirrelSQL */
	public SessionFactoryImpl getSessionFactoryImpl() {
		return (SessionFactoryImpl)HibernateUtil.getSession().getSessionFactory();
	}

	public static void closeSession() { HibernateUtil.getSession().close(); }

	public static void recreateDatabase() {
		Configuration config = HibernateUtil.getInitializedConfiguration();
		new SchemaExport(config).create(true, true);
	}

	public static Session beginTransaction() {
		Session hibernateSession = HibernateUtil.getSession();
		hibernateSession.beginTransaction();
		return hibernateSession;
	}

	public static void commitTransaction() { getSession().getTransaction().commit(); }

	public static void rollbackTransaction() { getSession().getTransaction().rollback(); }

	public static void main(String args[]) {
		HibernateUtil.recreateDatabase();
	}
}