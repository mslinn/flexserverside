/* Copyright 2009 Mike Slinn (mslinn@mslinn.com)
 *
 * Mike Slinn licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * $Id: ChanDefEvent.as 1133 2009-09-08 21:34:31Z mslinn $ */
 
package events {
  import flash.events.Event;


  public class ChanDefEvent extends Event {
  	/** Event type */
    public static const CHANNEL_DEFINITIONS:String = "com.mslinn.service.channelDef";
    
    public static const CHAN_RTMP:String          = "my-rtmp";
    public static const CHAN_NIO_STREAM:String    = "my-nio-stream";
    public static const CHAN_NIO_LONGPOLL:String  = "my-nio-longpoll";
    public static const CHAN_NIO_PIGGYBACK:String = "my-nio-piggyback";
    public static const CHAN_NIO_POLL:String      = "my-nio-poll";
    public static const CHAN_AMF_STREAM:String    = "my-amf-stream";
    public static const CHAN_AMF_LONGPOLL:String  = "my-amf-longpol";
    public static const CHAN_AMF_PIGGYBACK:String = "my-amf-piggyback";
    public static const CHAN_AMF_POLL:String      = "my-amf-poll";
    
    
    public var channelNames:Array;

    public function ChanDefEvent(channelNames:Array) {
      super(CHANNEL_DEFINITIONS, false, true);
      this.channelNames = channelNames;
    }

    override public function clone():Event {
      return new ChanDefEvent(channelNames);
    }
  }
}