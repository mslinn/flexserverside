package com.mslinn.lazy1 {
    import mx.collections.ArrayCollection;

    [RemoteClass(alias="com.mslinn.lazy1.SchoolClass")]
    [Managed]
    public class SchoolClass {
        public var id:int;
        public var name:String;
        public var students:ArrayCollection;
    }
}