/* Copyright 2009 Mike Slinn (mslinn@mslinn.com)
 *
 * Mike Slinn licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * $Id: DMS.as 1134 2009-09-08 21:40:48Z mslinn $ */
 
 package services {
    import events.ChanDefEvent;
    
    import mx.core.Application;
    import mx.controls.Alert;
    import mx.data.mxml.DataService;
    import mx.messaging.ChannelSet;
    import mx.messaging.channels.AMFChannel;
    import mx.messaging.channels.RTMPChannel;
    import mx.utils.URLUtil;
            
    [Bindable] public class DMS {
	    private static var instance:DMS = new DMS();
	            
        /** Defines preferred order of channel types */
        private const orderedChannels:Array = [
           ChanDefEvent.CHAN_RTMP, 
           ChanDefEvent.CHAN_NIO_STREAM,
           ChanDefEvent.CHAN_NIO_LONGPOLL,
           ChanDefEvent.CHAN_NIO_PIGGYBACK,
           ChanDefEvent.CHAN_NIO_POLL, 
           ChanDefEvent.CHAN_AMF_STREAM,
           ChanDefEvent.CHAN_AMF_LONGPOLL,
           ChanDefEvent.CHAN_AMF_PIGGYBACK,
           ChanDefEvent.CHAN_AMF_POLL
        ];
        
        /** Instance of this singleton */
        public var service:DataService;
        
        /** User selected channels, in order */
        private var channels:Object = new Object();

	    
	    public function DMS() { // create all channels so they are available to the user
            service = new DataService("StudentHibernate");
	    	channels[ChanDefEvent.CHAN_RTMP]          = new RTMPChannel(ChanDefEvent.CHAN_RTMP,          getEndpointRtmp());
            channels[ChanDefEvent.CHAN_NIO_STREAM]    = new  AMFChannel(ChanDefEvent.CHAN_NIO_STREAM,    getEndpointAmf("niostream"));
			channels[ChanDefEvent.CHAN_NIO_LONGPOLL]  = new  AMFChannel(ChanDefEvent.CHAN_NIO_LONGPOLL,  getEndpointNio("niolongpoll", 2080));
            channels[ChanDefEvent.CHAN_NIO_PIGGYBACK] = new  AMFChannel(ChanDefEvent.CHAN_NIO_PIGGYBACK, getEndpointNio("niopiggyback", 2080));
            channels[ChanDefEvent.CHAN_NIO_POLL]      = new  AMFChannel(ChanDefEvent.CHAN_NIO_POLL,      getEndpointNio("niopoll", 2080));
            channels[ChanDefEvent.CHAN_AMF_STREAM]    = new  AMFChannel(ChanDefEvent.CHAN_AMF_STREAM,    getEndpointAmf("amfstream"));
            channels[ChanDefEvent.CHAN_AMF_LONGPOLL]  = new  AMFChannel(ChanDefEvent.CHAN_AMF_LONGPOLL,  getEndpointAmf("amflongpoll"));
			channels[ChanDefEvent.CHAN_AMF_PIGGYBACK] = new  AMFChannel(ChanDefEvent.CHAN_AMF_PIGGYBACK, getEndpointAmf("amfpiggyback"));
			channels[ChanDefEvent.CHAN_AMF_POLL]      = new  AMFChannel(ChanDefEvent.CHAN_AMF_POLL,      getEndpointAmf("amfpoll"));
	    	addEventListener(ChanDefEvent.CHANNEL_DEFINITIONS, onChannelDef);
	    }
	    
	    /** Add user selected channel types to DMS channelSet in proper order */
	    private function onChannelDef(event:ChanDefEvent):void {
            if (event.channelNames.length==0) {
                Alert.show("Please select at least one channel");
                return;
            }
            var channelSet:ChannelSet = new ChannelSet();
            for each (var channel:String in orderedChannels)
                if (event.channelNames.indexOf(channel)>=0)
	               channelSet.addChannel(channels[channel]);
            service = new DataService("StudentHibernate");
		    service.channelSet = channelSet;
	    }
	    
		private function getEndpointAmf(serviceName:String):String {
	        var href:String = Application.application.url;
	        var domain:String = URLUtil.getProtocol(href) + "://" + URLUtil.getServerName(href);
	        // concatenate the web application context name
	        var endpoint:String = domain + "/" + href.split("/")[3];
	        // concatenate endpoint string for AMF
	        endpoint += "/messagebroker/" + serviceName;
	        return endpoint;
	    }
      
	    private function getEndpointNio(serviceName:String, port:int=0):String {
	        var href:String = Application.application.url;
	        var endpoint:String = URLUtil.getProtocol(href) + "://" + 
	          URLUtil.getServerName(href) + ":" + port + "/" + serviceName;
	        return endpoint;
        }      

	    private function getEndpointRtmp():String {
            var href:String = Application.application.url;
            var domain:String = "rtmp://" + URLUtil.getServerName(href) + ":2039";
            return domain;
        }      

	    public static function getInstance():DMS { return instance; }
    }
}