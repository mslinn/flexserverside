<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright 2009 Mike Slinn (mslinn@mslinn.com)
 *
 * Mike Slinn licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * $Id: Two.mxml 1133 2009-09-08 21:34:31Z mslinn $ -->
 
<mx:HBox 
    creationComplete="onCreationComplete()"
    filters="{[new DropShadowFilter(10, 45, 0x696464, 0.5, 8, 8)]}"
	styleName="bigBox"
	toolTip="An ItemPendingError is thrown on on each access of student.schoolClasses because the student's classes are lazily loaded."
	xmlns:mx="http://www.adobe.com/2006/mxml">
    
    <mx:Script>
        <![CDATA[
            import com.mslinn.lazy1.*; 
            import services.DMS;
            import mx.collections.ArrayCollection;
            import mx.collections.errors.ItemPendingError;
            import mx.controls.Alert;
            import mx.rpc.events.ResultEvent;
            import mx.rpc.Responder;

            [Bindable] private var dms:DMS = DMS.getInstance();
            [Bindable] private var students:ArrayCollection = new ArrayCollection();
            [Bindable] private var student:Student;
            [Bindable] private var enrolledStudents:ArrayCollection = new ArrayCollection();
            [Bindable] private var schoolClass:SchoolClass;
            [Bindable] private var schoolClasses:ArrayCollection = new ArrayCollection();
        

            private function labelSchoolClassName(item:Object):String {
	            var result:String;
	            if (item != null && item is SchoolClass)
	                result = (item as SchoolClass).name;
	            return result; 
	        }

            private function labelStudent(item:Object):String {
                var result:String;
                if (item != null && item is Student)
                    result = (item as Student).firstName + " " + (item as Student).lastName;
                return result; 
            }
            
            private function onCreationComplete():void {
                dms.service.addEventListener(ResultEvent.RESULT, onResultReceived);
            }
    
            private function onItemPendingClasses(data:Object):void {
                schoolClasses.removeAll();
                for each (var schoolClass:SchoolClass in data.body)
                    schoolClasses.addItem(schoolClass);
            }

            private function onItemPendingEnrolled(data:Object):void {
                enrolledStudents.removeAll();
                for each (var student:Student in data.body)
                    enrolledStudents.addItem(student);
            }

            private function onResultReceived(event:ResultEvent):void {
                if (event.token.message.destination=="StudentHibernate")
                    onResultClasses(event);
                else if (event.token.message.destination=="SchoolClassHibernate")
                    onResultEnrolled(event);
            }

            /** ItemPendingErrors are thrown because student.schoolClasses is lazily loaded. */
            private function onResultClasses(event:ResultEvent):void {
                student = studentList.selectedItem as Student;
                if (student==null)
                    return;
                classLabel.text = student.firstName + ' ' + student.lastName + '\'s classes';
                schoolClasses.removeAll();
                try { // idiom to trigger ItemPendingErrors - is there a more straightforward way?
                      // simply referencing student.schoolClasses returns null
                      // the For Each statement does something extra that causes the ItemPendingError to be thrown
                    for each (var schoolClass:SchoolClass in student.schoolClasses) {
                        // student.schoolClasses is null so this is not executed
                        schoolClasses.addItem(schoolClass);
                    }
                } catch (error:ItemPendingError) {
                    error.addResponder(new mx.rpc.Responder(onItemPendingClasses, null));
                } catch (error2:Error) { // discover any other errors
                    trace(error2.message);
                }
            }
        
            /** ItemPendingErrors are thrown because student.schoolClasses is lazily loaded. */
            private function onResultEnrolled(event:ResultEvent):void {
                schoolClass = classList.selectedItem as SchoolClass;
                if (schoolClass==null)
                    return;
                enrollment.text = schoolClass.name + ' students'; 
                enrolledStudents.removeAll();
                try { // idiom to trigger ItemPendingErrors - is there a more straightforward way?
                      // simply referencing schoolClass.schoolClasses returns null
                      // the For Each statement does something extra that causes the ItemPendingError to be thrown
                    for each (var student:Student in schoolClass.students) {
                        // student.schoolClasses is null so this is not executed
                        enrolledStudents.addItem(student);
                    }
                } catch (error:ItemPendingError) {
                    error.addResponder(new mx.rpc.Responder(onItemPendingEnrolled, null));
                } catch (error2:Error) { // discover any other errors
                    trace(error2.message);
                }
            }
        
            private function populateStudents():void {
            	try {
                    dms.service.fill(students, "all.students", []);
                } catch (error:Error) {
                	Alert.show(error.message);
                }
            }
        ]]>
    </mx:Script>
  <mx:VBox>
    <mx:Label text="Students" />
    <mx:List 
      change="onResultClasses(null)"
      dataProvider="{students}"
      height="100"
      id="studentList"
      labelFunction="labelStudent"
      width="200" />
    <mx:Button click="populateStudents()" label="Fetch students" />
  </mx:VBox>
  <mx:VBox>
    <mx:Label id="classLabel" text="The Student's classes" />
	<mx:HBox id="hbox" width="100%">		
	    <mx:List
            change="onResultEnrolled(null)"
	    	dataProvider="{schoolClasses}"
	        height="100"
	        id="classList"
	        labelFunction="labelSchoolClassName"
	        width="200" />
	</mx:HBox>
  </mx:VBox>
  <mx:VBox>
    <mx:Label id="enrollment" text="Enrolled Students" />
    <mx:List 
      dataProvider="{enrolledStudents}"
      height="100"
      id="classmateList"
      labelFunction="labelStudent"
      width="200" />
  </mx:VBox>
</mx:HBox>
