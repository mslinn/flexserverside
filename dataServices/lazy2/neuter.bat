REM this script removes LCDS proprietary JARs and SWCs from the demo project
REM run this program from the directory that the Eclipse project was installed into

set oldpath=path
path=..

call nick flex_libs\fds.swc
call nick flex_libs\fds_rb.swc
call nick flex_libs\playerfds.swc

call nick WebContent\WEB-INF\lib\flex-messaging-common.jar
call nick WebContent\WEB-INF\lib\flex-messaging-core.jar
call nick WebContent\WEB-INF\lib\flex-messaging-data.jar
call nick WebContent\WEB-INF\lib\flex-messaging-data-req.jar
call nick WebContent\WEB-INF\lib\flex-messaging-opt.jar
call nick WebContent\WEB-INF\lib\flex-messaging-proxy.jar
call nick WebContent\WEB-INF\lib\flex-messaging-remoting.jar

set path=oldpath
