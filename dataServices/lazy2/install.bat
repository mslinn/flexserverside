REM this script copies LCDS proprietary JARs and SWCs to the demo project
REM run this program from the directory that the DZONE LCDS refcard was installed into

set oldpath=path
path=..

REM modify the next 3 lines if you installed LCDS elsewhere:
if exist "c:\lcds\resources" set LCDS=C:\lcds\resources
if exist "c:\program files\lcds\resources" set LCDS=C:\Program Files\lcds\resources
if exist "c:\program files\adobe\lcds\resources" set LCDS=C:\Program Files\Adobe\lcds\resources

restore "%LCDS%\frameworks\libs"         fds.swc    flex_libs
restore "%LCDS%\frameworks\locale\en_US" fds_rb.swc flex_libs
restore "%LCDS%\frameworks\libs\player"  airfds.swc flex_libs

restore "%LCDS%\lib" flex-messaging-common.jar   WebContent\WEB-INF\lib
restore "%LCDS%\lib" flex-messaging-core.jar     WebContent\WEB-INF\lib
restore "%LCDS%\lib" flex-messaging-data.jar     WebContent\WEB-INF\lib
restore "%LCDS%\lib" flex-messaging-data-req.jar WebContent\WEB-INF\lib
restore "%LCDS%\lib" flex-messaging-opt.jar      WebContent\WEB-INF\lib
restore "%LCDS%\lib" flex-messaging-proxy.jar    WebContent\WEB-INF\lib
restore "%LCDS%\lib" flex-messaging-remoting.jar WebContent\WEB-INF\lib

set path=oldpath
