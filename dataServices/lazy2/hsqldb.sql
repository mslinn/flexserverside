CREATE TABLE class (
  id identity NOT NULL PRIMARY KEY,
  name varchar(255) default NULL
);
INSERT INTO class VALUES (1,'Geography'), (2,'Math'), (3,'Chemistry');

CREATE TABLE student (
  id identity NOT NULL PRIMARY KEY,
  firstName varchar(255) default NULL,
  lastName varchar(255) default NULL
);
INSERT INTO student VALUES (1,'Tim','Mack'), (2,'Mary','Slater'), (3,'Jim','Brown'), (4,'Adam','Keating');

CREATE TABLE student_class (
  studentId integer NOT NULL,
  classId integer NOT NULL,
  newfk integer,
  newfk2 integer,
  PRIMARY KEY (studentId,classId),
  FOREIGN KEY (newfk) REFERENCES student(ID),
  FOREIGN KEY (newfk2) REFERENCES class(ID)
);
INSERT INTO student_class (studentId, classId) VALUES (1,1), (2,1), (3,1), (1,2), (2,2), (2,3), (3,3), (4,3);
