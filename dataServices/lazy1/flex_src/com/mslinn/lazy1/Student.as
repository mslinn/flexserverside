package com.mslinn.lazy1 {
    import mx.collections.ArrayCollection;

    [RemoteClass(alias="com.mslinn.lazy1.Student")]
    [Managed]
    public class Student {
        public var id:int;
        public var firstName:String;
        public var lastName:String;
        public var schoolClasses:ArrayCollection;
    }
}