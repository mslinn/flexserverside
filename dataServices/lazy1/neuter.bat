REM this script removes LCDS proprietary JARs and SWCs from the demo project
REM run this program from the directory that the Eclipse project was installed into

set oldpath=path
path=..

del flex_libs\fds.swc
del flex_libs\fds_rb.swc
del flex_libs\playerfds.swc

del WebContent\WEB-INF\lib\flex-messaging-common.jar
del WebContent\WEB-INF\lib\flex-messaging-core.jar
del WebContent\WEB-INF\lib\flex-messaging-data.jar
del WebContent\WEB-INF\lib\flex-messaging-data-req.jar
del WebContent\WEB-INF\lib\flex-messaging-opt.jar
del WebContent\WEB-INF\lib\flex-messaging-proxy.jar
del WebContent\WEB-INF\lib\flex-messaging-remoting.jar

set path=oldpath
