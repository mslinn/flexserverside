package {

    [Managed]
    [RemoteClass(alias="com.mslinn.dmsAirSync.Contact")]
    public class Contact {
        public var contactId:int;
        public var firstName:String;
        public var lastName:String;
        public var title:String;
        public var phone:String;
        public var email:String;
    }
}