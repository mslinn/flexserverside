package com.mslinn.dmsAirSync;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ContactDAO extends BaseDAO {
	@SuppressWarnings("unchecked")
	public List findAll() {
		return getList("SELECT * FROM contact ORDER BY last_name, first_name");
	}

	@SuppressWarnings("unchecked")
	public List findByName(String name) {
		return getList(
				"SELECT * FROM contact WHERE UPPER(first_name) LIKE ? OR UPPER(last_name) LIKE ? ORDER BY last_name, first_name",
				new Object[] { "%" + name.toUpperCase() + "%",
						"%" + name.toUpperCase() + "%" });
	}

	public Object getContact(int contactId) {
		return getItem("SELECT * FROM contact WHERE contact_id=?", contactId);
	}

	public void create(Contact contact) throws DAOException {
		log("Creating contact: " + contact.getFirstName() + " " + contact.getLastName());
		int contactId = createItem(
				"INSERT INTO contact (first_name, last_name, title, phone, email) VALUES (?,?,?,?,?)",
				new Object[] { contact.getFirstName(), contact.getLastName(),
						contact.getTitle(), contact.getPhone(),
						contact.getEmail() });
		contact.setContactId(contactId);
		log("Contact created successfully - contactId: " + contact.getContactId());
	}

	public void update(Contact contact) throws DAOException, ConcurrencyException {
		log("Updating contact: " + contact.getContactId() + " "
				+ contact.getFirstName() + " " + contact.getLastName());
		int rows = executeUpdate(
				"UPDATE contact SET first_name=?, last_name=?, title=?, phone=?, email=? WHERE contact_id=?",
				new Object[] { contact.getFirstName(), contact.getLastName(),
				    contact.getTitle(), contact.getPhone(), contact.getEmail(), contact.getContactId() });
		if (rows == 0)
			throw new ConcurrencyException("Item not found");
		log("Contact updated successfully");
	}

	public void delete(Contact contact) throws DAOException, ConcurrencyException {
		log("Deleting contact: " + contact.getContactId() + " "
				+ contact.getFirstName() + " " + contact.getLastName());
		int rows = executeUpdate("DELETE FROM contact WHERE contact_id = ?",
				new Object[] { new Integer(contact.getContactId()) });
		if (rows == 0)
			throw new ConcurrencyException("Item not found");
		log("Contact deleted successfully");
	}

	@Override
	protected Object rowToObject(ResultSet rs) throws SQLException {
		Contact contact = new Contact();
		contact.setContactId(rs.getInt("contact_id"));
		contact.setFirstName(rs.getString("first_name"));
		contact.setLastName(rs.getString("last_name"));
		contact.setTitle(rs.getString("title"));
		contact.setPhone(rs.getString("phone"));
		contact.setEmail(rs.getString("email"));
		return contact;
	}

	private void log(String message) { System.out.println("# " + message); }
}