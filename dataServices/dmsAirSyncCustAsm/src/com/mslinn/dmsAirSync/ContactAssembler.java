package com.mslinn.dmsAirSync;

import java.util.List;
import java.util.Map;
import java.util.Collection;

import flex.data.DataSyncException;
import flex.data.assemblers.AbstractAssembler;

public class ContactAssembler extends AbstractAssembler {
	private ContactDAO dao = new ContactDAO();

	@Override
	public void createItem(Object newVersion) {
		dao.create((Contact) newVersion);
	}

	@Override
	public void deleteItem(Object prevVersion) {
		try {
			dao.delete((Contact)prevVersion);
		} catch (ConcurrencyException e) {
			int contactId = ((Contact) prevVersion).getContactId();
			System.err .println("*** DataSyncException when trying to delete contact id=" + contactId);
			throw new DataSyncException(dao.getContact(contactId), null);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection fill(List fillParameters) {
		if (fillParameters.size() == 0)
			return dao.findAll();

		String queryName = (String) fillParameters.get(0);
		if (queryName.equals("by-name"))
			return dao.findByName((String)fillParameters.get(1));

		return super.fill(fillParameters);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getItem(Map uid) {
		return dao.getContact(((Integer)uid.get("contactId")).intValue());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateItem(Object newVersion, Object prevVersion, List changes) {
		int contactId = ((Contact) newVersion).getContactId();
		try {
			dao.update((Contact)newVersion);
		} catch (ConcurrencyException e) {
			System.err.println("*** Throwing DataSyncException when trying to update contact id=" + contactId);
			throw new DataSyncException(dao.getContact(contactId), changes);
		}
	}
}