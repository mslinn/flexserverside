package com.mslinn.dmsAirSync;

public class Contact {
	private int contactId;
	private String email;
	private String firstName;
	private String lastName;
	private String phone;
	private String title;

	public int getContactId() { return contactId; }

	public String getEmail() { return email; }

	public String getFirstName() { return firstName; }

	public String getLastName() { return lastName; }

	public String getPhone() { return phone; }

	public String getTitle() { return title; }

	public void setContactId(int contactId) { this.contactId = contactId; }

	public void setEmail(String email) { this.email = email; }

	public void setFirstName(String firstName) { this.firstName = firstName; }

	public void setLastName(String lastName) { this.lastName = lastName; }

	public void setPhone(String phone) { this.phone = phone; }

	public void setTitle(String title) { this.title = title; }
}
