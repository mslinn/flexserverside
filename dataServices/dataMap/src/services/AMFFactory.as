/* Copyright 2007-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Significantly altered by Mike Slinn (mslinn@mslinn.com) */

package services {
	import com.adobe.utils.StringUtil;
	import model.Model;
    import mx.controls.Alert;
    import flash.events.Event;
	import mx.logging.ILogger;
	import mx.logging.Log;
    import mx.managers.CursorManager;
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	import mx.messaging.messages.RemotingMessage;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.mxml.Operation;
	import mx.rpc.remoting.mxml.RemoteObject;
    import mx.rpc.AsyncToken;


    /** An Operation's endpoint cannot be changed, so this factory should be called each time an endpoint changes. */
	public class AMFFactory {
	    private static var instance:AMFFactory;
	    private var username:String;
	    private var password:String;
        private static const logger:ILogger = Log.getLogger("RPC");
        public var stopWatch:StopWatch = new StopWatch();


	    public function AMFFactory() {
	        if (instance!=null)
	            throw new Error("AMFFactory is a singleton and should only be accessed through getInstance()");
	    }

	    public function createEmptyResultOperation(operationName:String):Operation {
	        var tmpOperation:Operation = new Operation();
	        tmpOperation.name = operationName;
	        return tmpOperation;
	    }

	    public function createOperation(operationName:String, successHandler:Function, faultHandler:Function=null, showBusyCursor:Boolean=true):Operation {
	        var operation:Operation = new Operation();
	        operation.name = operationName; 
	        operation.showBusyCursor = showBusyCursor;
	        operation.addEventListener(ResultEvent.RESULT, successHandler);
	        if (faultHandler!=null)
	            operation.addEventListener(FaultEvent.FAULT, faultHandler);
	        return operation;
	    }

	    public function createRemoteService(destination:String, operations:Object, endpoint:String="messagebroker/amf"):RemoteObject {
            var amfChannel:AMFChannel;
            if (StringUtil.endsWith(endpoint, "/amf"))
	            amfChannel = new AMFChannel("my-amf", endpoint);
    		else if (StringUtil.endsWith(endpoint, "/amflongpolling"))
            	amfChannel = new AMFChannel("my-longpolling-amf", endpoint);
            else
            	throw new Error("AMFFactory.createRemoteService() cannot create an AMFChannel for " + endpoint);

	        var remoteService:RemoteObject = new RemoteObject();
	        remoteService.destination = destination;
            remoteService.endpoint = endpoint;
	        remoteService.channelSet = new ChannelSet();
	        remoteService.channelSet.addChannel(amfChannel);
	        //remoteService.setCredentials(username, password);
	        if (operations!=null)
	            remoteService.operations = operations;

            remoteService.addEventListener(FaultEvent.FAULT,   defaultServiceFaultHandler);
            remoteService.addEventListener(ResultEvent.RESULT, defaultServiceResultHandler);
	        return remoteService;
	    }

	    protected function defaultServiceFaultHandler(event:FaultEvent):void {
	        CursorManager.removeBusyCursor();
            stopWatch.reportElapsedTime(faultMsg(event));
            Alert.show(faultMsg(event), "RPC Error");
	    }

	    protected function defaultServiceResultHandler(event:ResultEvent):void {
	    	CursorManager.removeBusyCursor();
            stopWatch.reportElapsedTime("AmfServices." + opName(event) + "()");
	    }

        public static function faultMsg(event:FaultEvent):String {
            var msg:String = "AmfServices." + opName(event) + "() statusCode " + event.statusCode + "\n";
            if (event.fault.faultCode.length>0)
                msg += "faultCode: " + event.fault.faultCode + "\n";
            if (event.message.destination && event.message.destination.length>0)
                msg += " to '" + event.message.destination + "'\n";
            if (event.fault.faultString!=null && event.fault.faultString.length>0)
                msg += "faultString: " + event.fault.faultString + "\n";
            if (event.fault.faultDetail!=null && event.fault.faultDetail.length>0)
                msg += "faultDetail: " + event.fault.faultDetail + "\n";
            if (event.fault.rootCause && event.fault.rootCause.rootCause!=null)
               msg += "rootCause: " + event.fault.rootCause.rootCause;
            return msg;
        }

	    public static function getInstance():AMFFactory {
	        if (instance==null)
	            instance = new AMFFactory();
	        return instance;
	    }

        private static function opName(event:Event):String {
            var name:String;
            if (event.currentTarget is Operation) {
                name = Operation(event.currentTarget).name;
            } else if (event.currentTarget is AsyncToken) {
                var asyncToken:AsyncToken = AsyncToken(event.currentTarget.token);
                if (asyncToken.message is RemotingMessage)
                    name = RemotingMessage(asyncToken.message).operation;
                else
                    name = "UNKNOWN.message";
            } else
                name = "UNKNOWN.currentTarget"
            return name;
        }

	    public function setUserCredentials(caller:Object, username:String, password:String):void {
	        //if (caller is SecurityManager) {
	            this.username = username;
	            this.password = password;
	        //}
	    }
	}
}