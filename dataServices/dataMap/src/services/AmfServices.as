/* Copyright 2007-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Significantly altered by Mike Slinn (mslinn@mslinn.com) */

 
 package services {
    import events.LoginEvent;
    import events.LogoutEvent;
    import flash.events.TimerEvent;
    import flash.utils.Timer;
    import model.Model;
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.logging.ILogger;
    import mx.logging.Log;
    import mx.managers.CursorManager;
	import mx.messaging.messages.ErrorMessage;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.remoting.mxml.RemoteObject;
	import mx.rpc.remoting.Operation;

	public class AmfServices {
        public var service:RemoteObject;
        private var _endpoint:String;
        private var operations:Object = new Object();
        private var rsFactory:AMFFactory = AMFFactory.getInstance();

        private var serviceDestination:String = "dataMap";
        
        /** Events are dispatched from here */
        private var eventSyncPoint:Object;
        private static const logger:ILogger = Log.getLogger("RPC");

        
	    public function AmfServices(eventSyncPoint:Object) {
	    	this.eventSyncPoint = eventSyncPoint;
	    }
	    
        public function get endpoint():String { return _endpoint; }

        /** Creates new RemoteObject every time the endpoint changes because an Operation's endpoint cannot be modified */        
	    public function set endpoint(newEndpoint:String):void {
	    	_endpoint = newEndpoint;
            operations = { // RPC function name:Operation name/value pairs
                // RPCs not associated with specific menu items:
                mapStuff:rsFactory.createOperation("mapStuff",  mapStuffSuccess),
            };
	    	service = rsFactory.createRemoteService(serviceDestination, operations, _endpoint);
	    }

        public function mapStuff(app:Model):void {
            if (service!=null) {
                rsFactory.stopWatch.startTimer();
                service.mapStuff();
            }
        }

        public function mapStuffSuccess(event:ResultEvent):void {
            CursorManager.removeBusyCursor();
            eventSyncPoint.dispatchEvent(event);
            rsFactory.stopWatch.reportElapsedTime("AmfServices.mapStuffSuccess()");
        }
    }
}