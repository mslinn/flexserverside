/* Copyright 2008 Mike Slinn (mslinn@mslinn.com)
 * 
 * Mike Slinn licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * $Id: DeData.as 691 2009-07-22 01:34:15Z mslinn $ */

package model {
    import flash.utils.ByteArray;
    
    [RemoteClass (alias="com.mslinn.DataMap")]
    public class DeData {
        public var anArray:Array;
        public var anArrayList:ArrayCollection;
        public var aBigDecimal:String;
        public var aBigInteger:String;
        public var aBoolean:Boolean;
        public var aByteArray:ByteArray;
        public var aCalendar:String;
        public var aChar:String;
        public var aCharArray:String;
        public var aDate:String;
        public var aDictionary:Object; // java.util.Dictionary
        public var aDocument:XML; // org.w3c.dom.Document     
        public var aDouble:Number;
        public var anEnum:String;
        public var aFloat:Number;
        public var anInteger:int; // short is same; If i < 0xF0000000 || i > 0x0FFFFFFF, the value is promoted to Number.
        public var aLong:Number;
        public var aMap:Object;
        public var aMapArray:Array;
        public var anObjectArray:Array;
        public var aString:String;
    }
}