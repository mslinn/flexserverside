package com.mslinn.service;

/* Copyright 2007-2009 Mike Slinn (mslinn@mslinn.com).
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

import java.io.Serializable;
import java.util.Date;
import org.apache.log4j.Logger;
import flex.messaging.FlexContext;
import flex.messaging.FlexSession;


/** <p>Flex data services creates a new SampleService instance for each 
 * call using the no-arguments constructor. Client data can be retrieved 
 * from the session context (flexSession). Each RPC that changes state 
 * must save state changes to flexSession.</p>
 * <p>All public methods and variables are exposed to Flex data services. 
 * If an error occurs server-side there might be no notification and the 
 * client might hang, unless it is caught and an Exception thrown. Thus 
 * every public method must wrap all code which might cause an Error, a 
 * Throwable or an Exception in a try/catch block, report and handle it, 
 * then throw an Exception to notify the client.</p>
 * @author Mike Slinn */
public class SampleService implements Serializable {
    /** Log4j logger */
    private static Logger logger = Logger.getLogger(SampleService.class);

    /** Serial version id of the class */
    private static final long serialVersionUID = -2671347758674441489L;

    /** Used to maintain user sessions */
    private FlexSession flexSession;


    public SampleService() throws Exception {
        super();
        try {
            flexSession = FlexContext.getFlexSession();
            if (flexSession == null) { // new session
                logger.info("No flex session currently exists");
            } else { // not a new session
              // attribute = flexSession.getAttribute("name");
            }
            new FlexClientSessionExplorer().displayVars();
        } catch (Exception ex) {
            logger.error("", ex);
            throw ex;
        }
    }

    /** Simple RPC */
    public Date login(String userId, String password) throws Exception {
      logger.debug("Logged attempted as " + userId);
      if (userId.compareTo("guest")!=0)
        throw new Exception("Invalid user id");
      return new Date();
    }

    /** Simple RPC */
    public Date byeBye() { 
      logger.debug("Logged out");
      return new Date();
    }
}