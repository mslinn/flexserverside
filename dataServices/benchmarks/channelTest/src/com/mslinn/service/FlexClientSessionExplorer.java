package com.mslinn.service;

/* Copyright 2007-2009 Mike Slinn (mslinn@mslinn.com).
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *    http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * $Id: FlexClientSessionExplorer.java 2188 2011-02-12 11:03:17Z mslinn $ */

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import flex.messaging.FlexContext;
import flex.messaging.FlexSession;
import flex.messaging.HttpFlexSession;
import flex.messaging.MessageClient;
import flex.messaging.client.FlexClient;
import java.security.Principal;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

/** This class is useful to learn about
 * {@code FlexContext}, a utility class that exposes the
 * current execution context for a Flex client's request.
 * {@code FlexContext} provides access to
 * {@code FlexSession} and {@code FlexClient} instances
 * associated with the current message being processed,
 * as well as global context via the {@code MessageBroker},
 * {@code ServletContext} and {@code ServletConfig} for the
 * application. Any, or all, of the properties exposed by
 * this class may be {@code null} depending upon the current
 * execution context.
 * @see http://livedocs.adobe.com/blazeds/1/javadoc/index.html?flex/messaging/FlexContext.html
 * @see http://livedocs.adobe.com/blazeds/1/javadoc/index.html?flex/messaging/FlexSession.html
 * @see http://livedocs.adobe.com/blazeds/1/javadoc/index.html?flex/messaging/client/FlexClient.html
 * @see http://livedocs.adobe.com/blazeds/1/javadoc/index.html?flex/messaging/MessageBroker.html
 * @see http://java.sun.com/javaee/5/docs/api/index.html?javax/servlet/ServletContext.html
 * @see http://java.sun.com/javaee/5/docs/api/index.html?javax/servlet/ServletConfig.html */
public class FlexClientSessionExplorer {
    /** Log4j logger */
  private static Logger logger =
    Logger.getLogger(FlexClientSessionExplorer.class);

  @SuppressWarnings("unchecked") public void displayVars() {
    /* The HttpServletResponse for the current request if the request is
     * via HTTP. Returns null if the client is using a non-HTTP channel. */
    HttpServletRequest httpServletRequest = FlexContext.getHttpRequest();
    logger.info("Flex client httpServletRequest=" + httpServletRequest);

    /* The ServletConfig for the current request, uses the last known
     * ServletConfig when the request is not via HTTP. */
    ServletConfig servletConfig = FlexContext.getServletConfig();
    logger.info("servletConfig=" + servletConfig);

    /* The ServletContext for the current web application. */
    ServletContext servletContext = FlexContext.getServletContext();
    logger.info("servletContext=" + servletContext);

    /* The FlexClient for the current request. */
    FlexClient flexClient = FlexContext.getFlexClient();

    /* The FlexSession for the current request. */
    FlexSession flexSession = FlexContext.getFlexSession();
    logger.info("flexSession id=" + flexSession.getId());

    /* The HttpServletRequest for the current request if it is
     * transporting a tunneled protocol. Returns null if the current
     * request protocol it not tunneled. */
    HttpServletRequest httpServletRequestTunnel =
    FlexContext.getTunnelHttpRequest();
    logger.info("httpServletRequestTunnel=" + httpServletRequestTunnel);

    /* The HttpServletResponse for the current request if the request is
     * via HTTP. Returns null if the using an non-HTTP channel. */
    HttpServletResponse httpServletResponse =
    FlexContext.getHttpResponse();
    logger.info("httpServletResponse=" + httpServletResponse);

    /* Indicates whether the current message being processed came from a
     * server peer in a cluster. */
    boolean peeredMsg = FlexContext.isMessageFromPeer();
    logger.info("peeredMsg=" + peeredMsg);

    /* Returns the principal associated with the session or client,
     * depending on whether perClientauthentication is being used.
     * If the client has not authenticated, principal will be null.
     * @see http://java.sun.com/javase/6/docs/api/index.html?java/security/Principal.html */
    Principal principal = FlexContext.getUserPrincipal();
    logger.info("principal=" + principal);

    /* Set the Principal on either the current FlexClient or FlexSession
     * depending upon whether perClientAuthentication is in use. */
    FlexContext.setUserPrincipal(null);

    /* Returns the unique Id for the FlexClient. */
    String clientId = flexClient.getId();
    logger.info("Flex client ID=" + clientId);

    /* Returns a snapshot of the FlexSessions associated with the
     * FlexClient when this method is invoked. This list is not
     * guaranteed to remain consistent with the actual list of active
     * FlexSessions associated with the FlexClient over time. */
    List<HttpFlexSession> flexClientSessions = flexClient.getFlexSessions();
    logger.info(flexClientSessions.size() + " flex client sessions");

    /* Returns true if the FlexClient is valid; false if it has been
     * invalidated. */
    boolean validClient = flexClient.isValid();
    logger.info("Flex client valid=" + validClient);

    /* Returns a snapshot of the FlexSessions associated with the
     * FlexClient when this method was invoked. This list is not
     * guaranteed to remain consistent with the actual list of active
     * FlexSessions associated with the FlexClient over time.  */
    int flexClientSessionCount = flexClient.getSessionCount();
    logger.info("Flex client session count=" + flexClientSessionCount);

    /* The number of subscriptions associated with this FlexClient. */
    int flexSubscriptions = flexClient.getSubscriptionCount();
    logger.info("Flex client subscription count=" + flexSubscriptions);

    /* For applications that use messaging, returns a snapshot of the
     * MessageClients (subscriptions) associated with the FlexClient
     * when this method is invoked. This list is not guaranteed to
     * remain consistent with the actual list of active MessageClients
     * associated with the FlexClient over time. */
    List<MessageClient> messageClients = flexClient.getMessageClients();
    logger.info("Flex client messageClients count=" + messageClients.size());

    /* Binds an attribute value for the FlexClient under the specified
     * name. */
    flexClient.setAttribute("name", "value");

    /* Returns the attribute bound to the specified name for the
     * FlexClient, or null if no attribute is bound under the name. */
    Object atttributeValue = flexClient.getAttribute("name");
    logger.info("Flex client attribute value=" + atttributeValue);

    /* Returns a snapshot of the names of all attributes bound to the
     * FlexClient. */
    Enumeration<String> clientAttributes = flexClient.getAttributeNames();
    logger.info("Flex client attributes=" + clientAttributes);

    /* The 'last use' time stamp for the Flex client, which may be the
     * current system time if the FlexClient has been idle but an
     * open connection from the client to the server exists. */
    long lastUseTimestamp = flexClient.getLastUse();
    logger.info("Flex client last used=" + new Date(lastUseTimestamp));

    /* Invalidates the Flex client. */
    //flexClient.invalidate();

    /* Returns true if the FlexClient is valid; false if it has been
     * invalidated. */
    boolean flexClientValid = flexClient.isValid();
    logger.info("Flex client is valid=" + flexClientValid);

    /* Creates or retrieves a FlexSession for the current HTTP request.
     * The HttpFlexSession wraps the underlying J2EE HttpSession.  */
    HttpFlexSession httpFlexSession =
      HttpFlexSession.getFlexSession(httpServletRequest);

    /* Returns the Id for the session (usually stored in a cookie). */
    String sessionId = httpFlexSession.getId();
    logger.info("Flex session ID=" + sessionId);
	}
}
