/* Copyright 2007-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Significantly altered by Mike Slinn (mslinn@mslinn.com) */
package services {
  import com.adobe.utils.StringUtil;
  
  import flash.events.Event;
  
  import mx.controls.Alert;
  import mx.logging.ILogger;
  import mx.logging.Log;
  import mx.managers.CursorManager;
  import mx.messaging.ChannelSet;
  import mx.messaging.channels.AMFChannel;
  import mx.messaging.channels.StreamingAMFChannel;
  import mx.messaging.messages.RemotingMessage;
  import mx.rpc.AsyncToken;
  import mx.rpc.events.FaultEvent;
  import mx.rpc.events.ResultEvent;
  import mx.rpc.remoting.mxml.Operation;
  import mx.rpc.remoting.mxml.RemoteObject;


  /** An Operation's endpoint cannot be changed, so this factory must
   * be called each time an endpoint changes. */
  public class AmfFactory {
    private static var instance:AmfFactory;
    private var username:String;
    private var password:String;
    private static const logger:ILogger = Log.getLogger("RPC");
    public var stopWatch:StopWatch = new StopWatch();

    public function AmfFactory() {
      if (instance != null)
        throw new Error("AmfFactory is a singleton and should " + 
          "only be accessed through getInstance()");
    }

    public function createEmptyResultOp(operationName:String):Operation {
      var tmpOperation:Operation = new Operation();
      tmpOperation.name = operationName;
      return tmpOperation;
    }

    public function createOperation(operationName:String, successHandler:Function=null,
      faultHandler:Function=null, showBusyCursor:Boolean=true):Operation {
      var operation:Operation = new Operation();
      operation.name = operationName;
      operation.showBusyCursor = showBusyCursor;
      if (successHandler != null)
        operation.addEventListener(ResultEvent.RESULT, successHandler);
      if (faultHandler != null)
        operation.addEventListener(FaultEvent.FAULT, faultHandler);
      return operation;
    }

    public function createRemoteService(destination:String, 
        operations:Object, endpoint:String="messagebroker/amf"):
        RemoteObject {
      var amfChannel:AMFChannel;
      if (StringUtil.endsWith(endpoint, "/amf"))
        amfChannel = new AMFChannel("my-amf", endpoint);
	  else if (StringUtil.endsWith(endpoint, "/amflongpolling"))
		  amfChannel = new AMFChannel("my-longpolling-amf", endpoint);
	  else if (StringUtil.endsWith(endpoint, "/streamingamf"))
		  amfChannel = new StreamingAMFChannel("my-streaming-amf", endpoint);
      else
        throw new Error("AmfFactory.createRemoteService() cannot " + 
          "create an AMFChannel for " + endpoint);
      var service:RemoteObject = new RemoteObject();
      service.destination = destination;
      service.endpoint = endpoint;
      service.channelSet = new ChannelSet();
      service.channelSet.addChannel(amfChannel);
      if (username)
        service.setCredentials(username, password);
      if (operations != null)
        service.operations = operations;
      service.addEventListener(FaultEvent.FAULT, defaultFaultHandler);
      service.addEventListener(ResultEvent.RESULT, defaultResultHandler);
      return service;
    }

    protected function defaultFaultHandler(event:FaultEvent):void {
      CursorManager.removeBusyCursor();
      stopWatch.reportElapsedTime(faultMsg(event));
      Alert.show(faultMsg(event), "RPC Error");
    }

    protected function defaultResultHandler(event:ResultEvent):void {
      CursorManager.removeBusyCursor();
      stopWatch.reportElapsedTime("AmfServices." + opName(event) + "()");
    }

    public static function faultMsg(event:FaultEvent):String {
      var msg:String = "AmfServices." + opName(event) + "() statusCode " + 
        event.statusCode + "\n";
      if (event.fault.faultCode.length > 0)
        msg += "faultCode: " + event.fault.faultCode + "\n";
      if (event.message.destination && event.message.destination.length>0)
        msg += " to '" + event.message.destination + "'\n";
      if (event.fault.faultString!=null && event.fault.faultString.length>0)
        msg += "faultString: " + event.fault.faultString + "\n";
      if (event.fault.faultDetail!=null && event.fault.faultDetail.length>0)
        msg += "faultDetail: " + event.fault.faultDetail + "\n";
      return msg;
    }

    public static function getInstance():AmfFactory {
      if (instance == null)
        instance = new AmfFactory();
      return instance;
    }

    private static function opName(event:Event):String {
      var name:String;
      if (event.currentTarget is Operation) {
        name = Operation(event.currentTarget).name;
      } else if (event is FaultEvent && event.currentTarget is RemoteObject) {
        var token:AsyncToken = AsyncToken(FaultEvent(event).token);
        name = RemotingMessage(token.message).operation;
      } else if (event.currentTarget is AsyncToken) { 
        var asyncToken:AsyncToken = AsyncToken(event.currentTarget.token);
        if (asyncToken.message is RemotingMessage)
          name = RemotingMessage(asyncToken.message).operation;
        else
          name = "UNKNOWN.message";
      } else
        name = "UNKNOWN.currentTarget"
      return name;
    }

    public function setUserCredentials(caller:Object, username:String, 
        password:String):void {
      this.username = username;
      this.password = password;
    }
  }
}