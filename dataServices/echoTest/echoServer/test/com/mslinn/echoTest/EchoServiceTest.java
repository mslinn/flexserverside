package com.mslinn.echoTest;

import static org.junit.Assert.*;

import org.junit.Test;
import com.mslinn.echo.Echo;

public class EchoServiceTest {
	Echo echoService = new Echo();

	@Test
	public void testEcho() {
		String actual = echoService.echo("hi");
		assertTrue(actual.endsWith("'hi'"));
	}
}
