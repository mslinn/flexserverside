package com.mslinn.echo;

import java.util.Date;

public class Echo {
	public int count;
	public String name;
	public boolean condition; 
	
	public String echo(String text) {
		return new Date().toString() + " Received '" + text + "'";
	}
}
