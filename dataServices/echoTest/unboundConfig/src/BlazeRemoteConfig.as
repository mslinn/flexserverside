package {
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	import mx.messaging.events.ChannelFaultEvent;

	[Bindable] public class BlazeRemoteConfig {
		public var amfChannelSet:ChannelSet = new ChannelSet();
		private var configFileName:String;
		
		public function BlazeRemoteConfig(configFileName:String) {
		    this.configFileName = configFileName;
		    loadConfiguration();
		}
		
		private function loadConfiguration():void {
		    var loader:URLLoader = new URLLoader();
		    loader.addEventListener(Event.COMPLETE, loadConfFileResultHandler);
		    loader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
		    loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		
		    var urlRequest:URLRequest = new URLRequest(configFileName);
		    try {
		    	// @see http://www.adobe.com/support/documentation/en/flashplayer/help/settings_manager04.html
		        loader.load(urlRequest);
		    } catch (error:Error) {
		        trace("Unable to load requested document.");
		    }               
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
		    Alert.show(event.toString());
		}
		
		private function loadConfFileResultHandler(event:Event):void {
			var loader:URLLoader = URLLoader(event.target);
		    var resultXml:XML = XML(loader.data);
		    parseConfigurationFile(resultXml);
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void {
		    Alert.show(event.toString());
		}
		
		/** Currently requires values for {server.name}, {server.port} and {context.root}.
		 * How to replace them with actual values - use a servlet to make text substitutions?
		 * Is there any way for Flex to provide these values at runtime? */
		private function parseConfigurationFile(confXml:XML):void {
		    if (confXml != null) {  // retrieve channel details from configuration file
		        parseChannels(XML(confXml.channels));
		    } else {
		        Alert.show("Invalid configuration file");
		    }
		}           
		
		private function parseChannels(xml:XML):void {
		    var _amfChannel:AMFChannel = null;
		
		    amfChannelSet = new ChannelSet();
		    var channels:XMLList = xml.elements("channel-definition");
		    for each (var channel:XML in channels) {
		        if (channel.@["class"] == "mx.messaging.channels.AMFChannel") {
		            var endpoint:XMLList = channel.endpoint;
		            trace(endpoint.toString());
		            if (endpoint==null)
		               throw new ChannelFaultEvent("Channel has no endpoint: " + channel.toString());
		            var url:String = channel.endpoint.@["url"];
		            if (url==null)
		               throw new ChannelFaultEvent("Channel has no URL endpoint: " + channel.toString());
		            if (url.indexOf("{server.name}")>=0 || 
		                url.indexOf("{server.port}")>=0 || 
		                url.indexOf("{context.root}")>=0)
		                throw new ChannelFaultEvent("URL contains unbound special variables. This parser requires a concrete URL.  URL was: " + url);
		            _amfChannel = new AMFChannel(channel.@id, url);
		            if (channel.properties.elements("polling-enabled")==true) {
		                _amfChannel.pollingEnabled = true;
		
		                var pollingInterval:* = channel.properties.elements("polling-interval-seconds");
		                if (pollingInterval!=null) // default is 3000 milliseconds
		                   _amfChannel.pollingInterval = pollingInterval * 1000;
		
		                /* TODO how is longpolling set up?
		                var waitInterval:* = channel.properties.elements("wait-interval-millis");
		                if (waitInterval!=null)
		                   _amfChannel.something = waitInterval;
		
		                var clientWaitInterval:* = channel.properties.elements("client-wait-interval-millis");
		                if (clientWaitInterval!=null)
		                   _amfChannel.something = clientWaitInterval;
		
		                var maxWaitingPollRequests:* = channel.properties.elements("max-waiting-poll-requests");
		                if (maxWaitingPollRequests!=null)
		                   _amfChannel.something = maxWaitingPollRequests; */
		            }
		        }
		        amfChannelSet.addChannel(_amfChannel);
		    }
	    }
	}
}