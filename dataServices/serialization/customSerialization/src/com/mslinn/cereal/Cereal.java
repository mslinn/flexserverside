package com.mslinn.cereal;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

 public class Cereal implements Externalizable {
	public int id; // read/write property
	public String name; // read/write property
	private boolean approved; // read-only property
	
	
    /** The Flex client is not allowed to alter this read-only property */
	public boolean getApproved(){ return approved; }
	
    // order must match order in Cereal.as writeExternal()
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException{
        approved = in.readBoolean();
        id = in.readInt();
        name = in.readUTF();
    }

    // order must match order in Cereal.as readExternal()
    public void writeExternal(ObjectOutput out) throws IOException {
    	out.writeBoolean(approved);
        out.writeInt(id);
        out.writeUTF(name);
    }	
}
