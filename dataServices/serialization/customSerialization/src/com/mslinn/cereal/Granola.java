package com.mslinn.cereal;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Granola extends Cereal {
	public boolean baked;
	
	
	// order must match order in Granola.as writeExternal()
    @Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException{
    	super.readExternal(in);
        baked = in.readBoolean();
    }

    // order must match order in Granola.as readExternal()
    @Override
	public void writeExternal(ObjectOutput out) throws IOException {
    	super.writeExternal(out);
    	out.writeBoolean(baked);
    }	
}
