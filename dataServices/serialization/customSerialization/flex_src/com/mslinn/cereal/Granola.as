package com.mslinn.cereal {
    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;

    [RemoteClass(alias="com.mslinn.cereal.Granola")]
    public class Granola extends Cereal {
        private var _moldy:Boolean;
        private var baked:Boolean;
        [Transient] public var fresh:Boolean;
        
        
        [Transient] public function get moldy():Boolean { return _moldy; }

        public function set moldy(value:Boolean):void { _moldy = value; }
        
        // order must match order in Granola.java readExternal()
        override public function readExternal(input:IDataInput):void {
            super.readExternal(input);
            baked = input.readBoolean();
        }
        
        // order must match order in Granola.java writeExternal()
        override public function writeExternal(output:IDataOutput):void {
            super.writeExternal(output);
            output.writeBoolean(baked);
        }
    }
}