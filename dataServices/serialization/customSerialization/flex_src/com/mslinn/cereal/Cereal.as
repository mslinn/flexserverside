package com.mslinn.cereal {
    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [RemoteClass(alias="com.mslinn.cereal.Cereal")]
    public class Cereal implements IExternalizable {
        public var id:int; // read/write property
        public var name:String; // read/write property
        private var _approved:Boolean; // read-only property
        
        
        /** The Flex client is not allowed to alter this read-only property */
        public function get approved():Boolean { return _approved; }
        
        // order must match order in Cereal.java writeExternal()
        public function readExternal(input:IDataInput):void {
            _approved = input.readBoolean();
            id = input.readInt();
            name = input.readUTF();
        }
    
        // order must match order in Cereal.java readExternal()
        public function writeExternal(output:IDataOutput):void {
            output.writeBoolean(_approved);
            output.writeInt(id);
            output.writeUTF(name);
        }
    }
}