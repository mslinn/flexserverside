package com.mslinn.cereal {

    [RemoteClass(alias="com.mslinn.cereal.CerealInt")]
    public class CerealInt {
        public var id:int; // read/write property
        public var name:String; // read/write property       
    }
}