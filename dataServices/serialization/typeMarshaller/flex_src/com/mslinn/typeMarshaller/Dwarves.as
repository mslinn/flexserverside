package com.mslinn.typeMarshaller {


    [RemoteClass(alias="com.mslinn.typeMarshaller.Dwarves")]
    public class Dwarves {
    	//  must these be instance variables?
        public static var BASHFUL:String = "BASHFUL";
        public static var DOC:String = "DOC";
        public static var DOPEY:String = "DOPEY";
        public static var GRUMPY:String = "GRUMPY";
        public static var HAPPY:String = "HAPPY";
        public static var SLEEPY:String = "SLEEPY";
        public static var SNEEZY:String = "SNEEZY";
    }
}