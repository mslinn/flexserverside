/* Copyright 2007-2009 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

package services {
  import events.EnumEvent;
  import flash.events.TimerEvent;
  import flash.utils.Timer;
  import mx.collections.ArrayCollection;
  import mx.controls.Alert;
  import mx.logging.ILogger;
  import mx.logging.Log;
  import mx.managers.CursorManager;
  import mx.messaging.messages.ErrorMessage;
  import mx.rpc.events.FaultEvent;
  import mx.rpc.events.ResultEvent;
  import mx.rpc.remoting.mxml.RemoteObject;
  import mx.rpc.remoting.Operation;


  public class AmfServices {
    public var service:RemoteObject;
    private var _endpoint:String;
    private var operations:Object = new Object();
    private var amfFactory:AmfFactory = AmfFactory.getInstance();
    private var serviceDestination:String = "tmarshal";
    /** Events are dispatched from here */
    private var eventSyncPoint:Object;
    private static const logger:ILogger = Log.getLogger("RPC");

    public function AmfServices(eventSyncPoint:Object) {
      this.eventSyncPoint = eventSyncPoint;
    }

    public function get endpoint():String { return _endpoint; }

    /** Create new RemoteObjects every time the endpoint changes
     * because an Operation's endpoint cannot be modified.
     * Implemented as object properties; a Dictionary would scale
     * better */
    public function set endpoint(newEndpoint:String):void {
      _endpoint = newEndpoint;
      operations = { // RPC function reference:Operation name/value pairs
          ordinalValue : amfFactory.createOperation("ordinalValue", ordinalValueSuccess),
          stringValue  : amfFactory.createOperation("stringValue",  stringValueSuccess) };
      service = amfFactory.createRemoteService(serviceDestination, 
        operations, _endpoint);
    }

    public function ordinalValue(string:String):void {
      if (service != null) {
        amfFactory.stopWatch.startTimer();
        service.ordinalValue(string);
      }
    }

    public function ordinalValueSuccess(event:ResultEvent):void {
      CursorManager.removeBusyCursor();
      var result:* = event.result;
      var enumEvent:EnumEvent = new EnumEvent(result);
      eventSyncPoint.dispatchEvent(enumEvent);
      amfFactory.stopWatch.reportElapsedTime("AmfServices.ordinalValue()");
    }

    public function stringValue(string:String):void {
      if (service != null) {
        amfFactory.stopWatch.startTimer();
        service.stringValue(string);
      }
    }

    public function stringValueSuccess(event:ResultEvent):void {
      CursorManager.removeBusyCursor();
      var result:* = event.result;
      var enumEvent:EnumEvent = new EnumEvent(result);
      eventSyncPoint.dispatchEvent(enumEvent);
      amfFactory.stopWatch.reportElapsedTime("AmfServices.stringValue()");
    }
  }
}