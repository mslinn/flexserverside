package com.mslinn.typeMarshaller;

public enum Dwarves {
    BASHFUL, DOC, DOPEY, GRUMPY, HAPPY, SLEEPY, SNEEZY
}
