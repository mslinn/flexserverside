package com.mslinn;

/* Copyright 2009 Mike Slinn (mslinn@mslinn.com).
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

import java.io.Serializable;
import org.apache.log4j.Logger;

import com.mslinn.typeMarshaller.Dwarves;


/** <p>Flex data services creates a new EnumService instance for each 
 * call using the no-arguments constructor. Client data can be retrieved 
 * from the session context (flexSession). Each RPC that changes state 
 * must save state changes to flexSession.</p>
 * <p>All public methods and variables are exposed to Flex data services. 
 * If an error occurs server-side there might be no notification and the 
 * client might hang, unless it is caught and an Exception thrown. Thus 
 * every public method must wrap all code which might cause an Error, a 
 * Throwable or an Exception in a try/catch block, report and handle it, 
 * then throw an Exception to notify the client.</p>
 * @author Mike Slinn */
public class EnumService implements Serializable {
    /** Log4j logger */
    private static Logger logger = Logger.getLogger(EnumService.class);
    private static final long serialVersionUID = -2671347758674441442L;


    /** TypeMarshaller will look up the ActionScript string into the appropriate Dwarves enum value.
     * @return the ordinal value of the Dwarf */
    public int ordinalValue(Dwarves dwarf) {
      logger.debug("ordinalValue(" + dwarf + ") returning " + dwarf.ordinal());
      return dwarf.ordinal();
    }

    /** TypeMarshaller will convert the returned enum to a string 
     * representation and pass it back as the RPC result.  This call is equivalent to
     * <pre>public Dwarves stringValue(Dwarves dwarf) {
     *    return dwarf.toString();
     *}</pre> */
    public Dwarves stringValue(Dwarves dwarf) { 
    	logger.debug("stringValue(" + dwarf + ") returning " + dwarf.toString());
      	return dwarf; 
    }
}