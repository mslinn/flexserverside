import java.util.List;
import java.util.Map;
import java.util.Collection;
import flex.data.assemblers.AbstractAssembler;

public class MyAssembler extends AbstractAssembler {

	@SuppressWarnings("unchecked")
	@Override
	public Collection fill(List fillParameters) { }

	@SuppressWarnings("unchecked")
	@Override
	public Object getItem(Map uid) { }

	@Override
	public void createItem(Object newVersion) { }

	@SuppressWarnings("unchecked")
	@Override
	public void updateItem(Object newVersion, Object prevVersion, List changes) { }

	@Override
	public void deleteItem(Object prevVersion) { }
}