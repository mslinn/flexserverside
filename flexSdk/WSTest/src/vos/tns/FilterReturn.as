package vos.tns {

    /** @author Mike Slinn 3/19/10 */
    public class FilterReturn {
        public var FoundProfanity:Boolean;
        public var ProfanityCount:int;
        public var CleanText:String;
        
        public function toString():String {
            return "FoundProfanity: " + FoundProfanity + "\n" +
                   "ProfanityCount: " + ProfanityCount + "\n" +
                   "CleanText: " + CleanText; 
        }
    }
}