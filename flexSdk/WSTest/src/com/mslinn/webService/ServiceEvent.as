package com.mslinn.webService {
    import flash.events.Event;


    /** @author Mike Slinn 3/19/10 */
    public class AlbumInfoEvent extends Event {
        public static const ALBUM_INFO:String = "searchArtists";
        public var info:String;
        
        public function AlbumInfoEvent(info:String) {
            super(ALBUM_INFO, false, false);
        }
        
        public override function clone():Event {
            var newEvent:AlbumInfoEvent = new AlbumInfoEvent(info);
            return newEvent;
        }
    }
}