package com.mslinn.webService {
    import com.adobe.utils.StringUtil;
    import mx.rpc.wsdl.WSDL;
    import mx.rpc.xml.SchemaTypeRegistry;
    import com.mslinn.util.RegexMap;

    /** Scans a loaded WSDL v1.1 file for operation output namespaces, then registers all VOs for each corresponding SOAP type.
    * Assumes that the VO package name is the same as the SOAP type namespace.
    * This class will require modification in order to support WSDL v2.
    * @author Mike Slinn 3/19/10 */
    public class ServiceWsdl {
        private var _schemaTypeRegistry:SchemaTypeRegistry;
        private var wsdlNS:Namespace;
        private var sNS:Namespace;
        
        /** _wsdl.xml contains the complete WSDL XML, including all imports */
        private var _wsdl:WSDL;
        private var _wsdlUrl:String;
        private var _typeMaps:RegexMap;


        /** This class constructor must be invoked after the WSDL has been loaded. */
        public function ServiceWsdl(wsdlUrl:String, wsdl:WSDL, typeMaps:RegexMap) {
            _wsdlUrl = wsdlUrl;
            _wsdl = wsdl;
            _typeMaps = typeMaps;
            wsdlNS = _wsdl.xml.namespace();
            sNS = _wsdl.xml.wsdlNS::types[0].*::schema.namespace();
            _schemaTypeRegistry = SchemaTypeRegistry.getInstance();
        }
        
        /** Automatically register the mappings for all complex soap elements to value objects in subclass.
        * The SOAP element namespace is used as the VO's package name. */
        public function mapSoapElementsToVOs():ServiceWsdl {
            var outputNSArray:Array = outputNSs;
            var schemaElements:XMLList = _wsdl.xml.wsdlNS::types[0].sNS::schema.sNS::element;
            for each (var schemaElement:XML in schemaElements) {
                var name:String = schemaElement.@name;
                var elements:XMLList = schemaElement.sNS::complexType..sNS::element;
                for each (var element:XML in elements) {
                    var elementNS:String = getNamespace(element.@type);
                    if (outputNSArray.indexOf(elementNS)>=0)
                        registerElement(element);
                }
            }
            return this;
        }
        
        public function get wsdlUrl():String { return _wsdlUrl; }
        
        public function get wsdl():WSDL { return _wsdl; }
        
        public static function getNamespaceDefinition(element:XML, nsPrefix:String):Namespace {
            var nsds:Array;
            do {
                nsds = element.namespaceDeclarations();
                for each (var ns:Namespace in nsds) {
                    if (ns.prefix==nsPrefix)
                        return ns;
                }
                element = element.parent();
            } while(element);
            throw new Error("Namespace " + nsPrefix + " is not defined.");
        }

        /** @return array containing all namespaces used for operation outputs, without any duplicates */
        private function get outputNSs():Array {
            var nss:Array = [];
            var portTypes:XMLList = _wsdl.xml.wsdlNS::portType;
            var operations:XMLList = _wsdl.xml.wsdlNS::portType.wsdlNS::operation;
            var outputs:XMLList = _wsdl.xml.wsdlNS::portType.wsdlNS::operation.wsdlNS::output;
            for each (var output:XML in outputs) {
                var ns:String = getNamespace(output.@message);
                if (nss.indexOf(ns)<0)
                    nss.push(ns);
            }
            return nss;
        }
        
        /** @param soapType SOAP element name to look up
        * @return fully qualified SOAP type for a given SOAP element name, in the form namespace:name */
        private function soapTypeForElementName(soapType:String):String {
            var wsdlNS:Namespace = _wsdl.xml.namespace();
            var sNS:Namespace = _wsdl.xml.wsdlNS::types[0].*::schema.namespace();
            var element:XMLList = _wsdl.xml.wsdlNS::types[0].sNS::schema.sNS::element.sNS::complexType..sNS::element.(@name==soapType);
            var type:String = element[0].@type;
            return type;
        }
        
        /** Registers the element's type in the SOAP schema type registry.
        * Ensure that the value object is called the same as the SOAP type.
        * The value object's package must match the namespace name. */
        private function registerElement(element:XML):void {
            var typeName:String = removeNamespace(element.@type);
            var targetNamespace:String = _wsdl.targetNamespace.uri;
            var qName:QName = new QName(targetNamespace, typeName);

            var typeNS:String = getNamespace(element.@type);
            var qType:String = typeNS + "." + typeName;
            var targetQType:String = _typeMaps.match(qType);
            trace("Registering the SOAP type for the SOAP element named '" + element.@name + "' to VO type '" + targetQType + "'.");
            _schemaTypeRegistry.registerClass(qName, targetQType);
        }
        
        /** @return namespace for a fully qualified SOAP type */
        private function getNamespace(fqName:String):String {
            var fqNameParts:Array = fqName.split(":");
            var ns:String = fqNameParts.length>1 ? fqNameParts[0] : "";
            return ns;
        }
        
        /** @return remaining string after removing the namespace prefix, if present */
        private function removeNamespace(fqName:String):String {
            var fqNameParts:Array = fqName.split(":");
            var name:String = fqNameParts.length>1 ? fqNameParts[1] : fqNameParts[0];
            return name;
        }
    }
}