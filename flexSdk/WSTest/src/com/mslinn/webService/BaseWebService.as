package com.mslinn.webService {
    import flash.events.EventDispatcher;
    import mx.controls.Alert;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.soap.WebService;
    import mx.rpc.soap.LoadEvent;
    import com.mslinn.util.RegexMap;
    

    [Event(name="wsdlLoad", type="mx.rpc.events.WSDLLoadEvent")]
    /** This class delegates WSDL operations to ServiceWsdl, which only supports WSDL v1.1.
    * If you need to support WSDL v2, you need only to modify ServiceWsdl.
     * @author Mike Slinn 3/19/10 */
    public class BaseWebService extends EventDispatcher {
        protected var service:WebService;
        
        /** Protocol, domain and path to web service */
        protected var baseWsUrl:String;
        
        /** name of web service */
        protected var serviceName:String;

        /** Support class per web service */
        protected var serviceWsdl:ServiceWsdl;
        
        /** Optional overrides for mapping SOAP element types to fully qualified VO names. */
        protected var typeMaps:RegexMap = new RegexMap();
        

        public function BaseWebService(baseWsUrl:String, serviceName:String) {
            this.baseWsUrl = baseWsUrl;
            this.serviceName = serviceName;
            service = new WebService();
            service.addEventListener(LoadEvent.LOAD, onWsdlLoad);
            service.loadWSDL(wsdlUrl);
            service.addEventListener(FaultEvent.FAULT, faultHandler, false, 0, false);
        }

        public function faultHandler(event:FaultEvent):void {
            Alert.show(event.fault.faultString);
        }
        
        public function get wsdlUrl():String {
            return baseWsUrl + "/" + serviceName + "?wsdl";
        }

        /** Propagate the LoadEvent after all of the web service soap element/vo mappings are registered */
        protected function onWsdlLoad(event:LoadEvent):void {
            serviceWsdl = new ServiceWsdl(wsdlUrl, event.wsdl, typeMaps).mapSoapElementsToVOs();
            dispatchEvent(event);
        }
    }
}