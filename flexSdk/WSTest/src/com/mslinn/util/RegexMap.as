package com.mslinn.util {
    import flash.utils.Dictionary;
    import flash.utils.getQualifiedClassName;

    /** Maps key to values; keys may either be regular expressions or simple strings.
    * If a key is a String then the value must also be a String.
    * If a key is a RegExp then the returned value may be constructed using substring replacement.
    * The search is guaranteed to be performed in the order that the key/value pairs were pushed.
    * If the default value is not specified, the default behavior is to echo the match string. 
    * @see http://livedocs.adobe.com/flex/3/langref/String.html#replace()
    * @author Mike Slinn (mslinn@mslinn.com) */
    public class RegexMap {
        private var map:Array = [];
        
        /** matches entire string as a capturing group */
        private var defaultKey:* = /(.*)/;
        
        /** return entire string unchanged */
        private var defaultValue:String = "$&";
        private var enableReplacementCodesDefault:Boolean = true;
        
        
        /** @param key String or RexExp to match
        * @param value String to substitute; can contain special $ replacement codes 
        * @param enableReplacementCodes is ignored unless key is a RegExp; if so, then String.replace() is used to substitute matched text. */
        public function push(key:*, value:String, enableReplacementCodes:Boolean=true):RegexMap {
            if (!key is String && !key is RegExp)
                throw new Error("RegexMap: invalid key type: " + getQualifiedClassName(key));

            map.push(new KeyValuePair(key, value, enableReplacementCodes));
            return this;
        }
        
        /** @return value of first matching key; if key and value are both regular expressions, 
        * the returned value will be constructed using substitution if specified. 
        * The default value is returned if there is no match. */
        public function match(searchKey:String):String {
            var newValue:String;
            for each (var kvPair:KeyValuePair in map) {
                var key:* = kvPair.key;
                if (key is String)
                    if (key==searchKey)
                        return kvPair.value;
                    else
                        continue;
                // key must be a RegExp
                var keyRE:RegExp = RegExp(key);
                if (keyRE.test(searchKey)) {
                    if (!kvPair.enableReplacementCodes is String)
                        return kvPair.value;
                    // key must be a RegExp and string replacement is enabled
                    newValue = searchKey.replace(keyRE, kvPair.value);
                    return newValue;
                }
            }
            // searchKey did not match, use default value
            if (enableReplacementCodesDefault) {
                newValue = searchKey.replace(defaultKey, defaultValue);
                return newValue;
            } else {
                return defaultValue;
            }
        }
        
        /** @param value String to substitute; can contain special $ replacement codes */
        public function setDefaultValue(value:String, enableReplacementCodes:Boolean=true):RegexMap {
            defaultValue = value;
            enableReplacementCodesDefault = enableReplacementCodes;
            if (defaultValue.toString().indexOf("$&")>=0 && !enableReplacementCodesDefault)
                trace("Did you make an error?  The defaultValue is '" + defaultValue + 
              "'.  It seems that you intended to use regular expressions to make a string substitution, but defaultEnableReplacementCodes is not enabled.");
            return this;
        }
    }
}

class KeyValuePair {
    public var key:*;
    public var value:*;
    public var enableReplacementCodes:Boolean;
    
    public function KeyValuePair(key:*, value:*, enableReplacementCodes:Boolean=true) {
        this.key = key;
        this.value = value;   
        this.enableReplacementCodes = enableReplacementCodes;   
        if (key.toString().indexOf("$&")>=0 && !enableReplacementCodes)
            trace("Did you make an error?  The value is '" + value + 
                  "'.  It seems that you intended to use regular expressions to make a string substitution, but enableReplacementCodes is not enabled.");
    } 
}