package com.custom.webService {
    import mx.controls.Alert;
    import mx.core.Application;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.soap.Operation;
    import mx.rpc.xml.SchemaTypeRegistry;
    
    import com.mslinn.webService.BaseWebService;
    import vos.tns.FilterReturn;
    
    /** @see http://wiki.cdyne.com/wiki/index.php?title=Profanity_Filter
     * WSDL is at http://ws.cdyne.com/ProfanityWS/Profanity.asmx?wsdl
     * 
     * Sample request:
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:prof="http://ws.cdyne.com/ProfanityWS/Profanity.asmx">
   <soapenv:Header/>
   <soapenv:Body>
      <prof:SimpleProfanityFilter>
         <prof:Text>The seven words you can never say on television</prof:Text>
      </prof:SimpleProfanityFilter>
   </soapenv:Body>
</soapenv:Envelope>

    Response is:
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
   <soap:Body>
      <SimpleProfanityFilterResponse xmlns="http://ws.cdyne.com/ProfanityWS/Profanity.asmx">
         <SimpleProfanityFilterResult>
            <FoundProfanity>false</FoundProfanity>
            <ProfanityCount>0</ProfanityCount>
            <CleanText>The seven words you can not say on television</CleanText>
         </SimpleProfanityFilterResult>
      </SimpleProfanityFilterResponse>
   </soap:Body>
</soap:Envelope> 
     * @author Mike Slinn 3/19/10 */
    [Bindable] public class SimpleProfanityService extends BaseWebService {
        
        public function SimpleProfanityService() {
            super("http://ws.cdyne.com/ProfanityWS", "Profanity.asmx");
            typeMaps.setDefaultValue("vos.$&");
            var op:Operation = Operation(service.SimpleProfanityFilter);
            op.addEventListener(ResultEvent.RESULT, resultHandler, false, 0, false);
            op.addEventListener(FaultEvent.FAULT, opFaultHandler, false, 0, false);
        }

        /** Invoke web service operation */
        public function SimpleProfanityFilter(text:String):void {
            service.SimpleProfanityFilter(text);
        }
             
        private function opFaultHandler(event:FaultEvent):void {
            Alert.show(event.fault.message);
        }

        private function resultHandler(event:ResultEvent):void {
            var simpleProfanityFilterResult:FilterReturn = event.result as FilterReturn;
            var resultEvent:SimpleProfanityServiceEvent = new SimpleProfanityServiceEvent(simpleProfanityFilterResult);
            Application.application.dispatchEvent(resultEvent);
        }
    }
}