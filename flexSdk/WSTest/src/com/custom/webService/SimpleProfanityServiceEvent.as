package com.custom.webService {
    import flash.events.Event;
    import vos.tns.FilterReturn;


    /** @author Mike Slinn 3/19/10 */
    public class SimpleProfanityServiceEvent extends Event {
        public static const TEXT:String = "SimpleProfanityFilter";
        public var result:FilterReturn;
        
        public function SimpleProfanityServiceEvent(result:FilterReturn) {
            super(TEXT, false, false);
            this.result = result;
        }
        
        public override function clone():Event {
            var newEvent:SimpleProfanityServiceEvent = new SimpleProfanityServiceEvent(result);
            return newEvent;
        }
    }
}