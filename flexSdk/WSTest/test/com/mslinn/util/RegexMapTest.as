package com.mslinn.util {
    import flexunit.framework.TestCase;
    import com.mslinn.util.RegexMap;
    
    
    /** If the fully qualified soap type name exactly matches a specified key then the corresponding value/VO name is returned; 
    * If enableReplacementCodes is set then the SOAP type name is transformed into the desired VO class name by substituting 
    * captured groups defined in the matching key withspecial replacement codes in the corresponding value.
    * If the fully qualified soap type name does not matches any keys then the default value is used, taking into account 
    * enableReplacementCodesDefault.  If the default value is not overridden, the VO class name is set equal to the fully 
    * qualified SOAP type name.
    * @author Mike Slinn (mslinn@mslinn.com) */
    public class RegexMapTest extends TestCase {
    	private var typeMaps:RegexMap;
    	
        public function testRegExp():void {
            typeMaps = new RegexMap();
            typeMaps.push("tns.FilterReturn", "vos.tns.FilterReturn");
            var actual:String = typeMaps.match("tns.blah");
            assertEquals("testRegExp default 1", "tns.blah", actual);

            actual = typeMaps.match("tns.FilterReturn");
            assertEquals("testRegExp good 1", "vos.tns.FilterReturn", actual);
            
            typeMaps.push(/(tns.*)/, "vos.$&");
            actual = typeMaps.match("blah.blah");
            assertEquals("testRegExp default 2", "blah.blah", actual);

            typeMaps.setDefaultValue("x.y", false);
            actual = typeMaps.match("blah.blah");
            assertEquals("testRegExp default 3", "x.y", actual);

            typeMaps.setDefaultValue("x.y.$&", true);
            actual = typeMaps.match("blah.blah");
            assertEquals("testRegExp default 3", "x.y.blah.blah", actual);

            actual = typeMaps.match("tns.FilterReturn");
            assertEquals("testRegExp good  2", "vos.tns.FilterReturn", actual);

            actual = typeMaps.match("tns.ShouldMatch");
            assertEquals("testRegExp good replaceText 1", "vos.tns.ShouldMatch", actual);
        }
    }
}