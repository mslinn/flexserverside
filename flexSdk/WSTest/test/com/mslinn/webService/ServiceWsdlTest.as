package com.mslinn.webService {
    import flexunit.framework.TestCase;
    
    
    /** @author Mike Slinn (mslinn@mslinn.com) */
    public class ServiceWsdlTest extends TestCase {
        private var xml:XML = 
           <wayOut xmlns:ns1="wilma" xmlns:ns2="betty" handle="ns2:wife">
              <outer id="outer" xmlns:ns1="fred" handle="ns2:neighbor">
                 <inner name="inner1" xmlns:ns2="barney" handle="ns2:husband" />
                 <inner name="inner2" handle="ns1:dino" />
              </outer>
           </wayOut>;
    	
        public function testNamespaceDefinition():void {
            var nodeOuter:XML = xml.outer[0];
            var ns:Namespace = ServiceWsdl.getNamespaceDefinition(nodeOuter, "ns1");
            assertEquals("Namespace 1 uri for outer", "fred", ns.uri);
            ns = ServiceWsdl.getNamespaceDefinition(nodeOuter, "ns2");
            assertEquals("Namespace 2 uri for outer", "betty", ns.uri);

            var nodeBarney:XML = xml.outer.inner.(@name=="inner1")[0];
            ns = ServiceWsdl.getNamespaceDefinition(nodeBarney, "ns1");
            assertEquals("Namespace 1 uri for barney", "fred", ns.uri);
            ns = ServiceWsdl.getNamespaceDefinition(nodeBarney, "ns2");
            assertEquals("Namespace 2 uri for barney", "barney", ns.uri);

            var nodeDino:XML = xml.outer.inner.(@name=="inner2")[0];
            ns = ServiceWsdl.getNamespaceDefinition(nodeDino, "ns1");
            assertEquals("Namespace 1 uri for Dino", "fred", ns.uri);
            ns = ServiceWsdl.getNamespaceDefinition(nodeDino, "ns2");
            assertEquals("Namespace 2 uri for Dino", "betty", ns.uri);
        }
    }
}