package com.mslinn;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.hibernate.impl.SessionFactoryImpl;

/** Small wrapper class to start Hibernate with Annotations enabled.
 * @see http://www.hibernate.org/hib_docs/annotations/reference/en/html/ch01.html
 * @see http://www.hibernate.org/hib_docs/annotations/reference/en/html/entity.html */
public class HibernateUtil {

private static final SessionFactory sessionFactory;

    static {
        try {
            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Log exception!
            throw new ExceptionInInitializerError(ex);
        }
    }

	/** For SquirrelSQL */
	public SessionFactoryImpl getSessionFactoryImpl() {
		return (SessionFactoryImpl)HibernateUtil.getSession().getSessionFactory();
	}

    public static Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }
}
