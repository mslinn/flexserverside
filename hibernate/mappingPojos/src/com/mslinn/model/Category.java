package com.mslinn.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "category", catalog = "craigmeister")
public class Category implements Serializable {
	private static final long serialVersionUID = 5717820870919660400L;
	private Long id;
	private String first;
	private String second;

	public Category() { }

	public Category(String primary, String secondary) {
		this.first = primary;
		this.second = secondary;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() { return this.id; }

	public void setId(Long id) { this.id = id; }

	@Column(name = "first", nullable = false, length = 45)
	public String getFirst() { return this.first; }

	public void setFirst(String primary) { this.first = primary; }

	@Column(name = "second", nullable = false, length = 45)
	public String getSecond() { return this.second; }

	public void setSecond(String secondary) { this.second = secondary; }
}
