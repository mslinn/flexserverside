package com.mslinn.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "userz", catalog = "craigmeister")
public class Userz implements java.io.Serializable {
	private static final long serialVersionUID = -4628536651004218018L;
	private Integer id;
	private Boolean active;
	private Boolean autoLogin;
	private String email;
	private Boolean emailOk;
	private Date lastLogin;
	private String password;
	private Boolean privilege;
	private Date signUp;
	private Boolean titlesOnly;
	private String subject;

	public Userz() { }

	public Userz(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public Userz(Boolean active, Boolean autoLogin, String email,
			Boolean emailOk, Date lastLogin, String password,
			Boolean privilege, Date signUp, Boolean titlesOnly, String subject) {
		this.active = active;
		this.autoLogin = autoLogin;
		this.email = email;
		this.emailOk = emailOk;
		this.lastLogin = lastLogin;
		this.password = password;
		this.privilege = privilege;
		this.signUp = signUp;
		this.titlesOnly = titlesOnly;
		this.subject = subject;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() { return this.id; }

	public void setId(Integer id) { this.id = id; }

	@Column(name = "active")
	public Boolean getActive() { return this.active; }

	public void setActive(Boolean active) { this.active = active; }

	@Column(name = "autoLogin")
	public Boolean getAutoLogin() { return this.autoLogin; }

	public void setAutoLogin(Boolean autoLogin) { this.autoLogin = autoLogin; }

	@Column(name = "email", nullable = false, length = 100)
	public String getEmail() { return this.email; }

	public void setEmail(String email) { this.email = email; }

	@Column(name = "emailOk")
	public Boolean getEmailOk() { return this.emailOk; }

	public void setEmailOk(Boolean emailOk) { this.emailOk = emailOk; }

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastLogin", length = 19)
	public Date getLastLogin() { return this.lastLogin; }

	public void setLastLogin(Date lastLogin) { this.lastLogin = lastLogin; }

	@Column(name = "password", nullable = false, length = 45)
	public String getPassword() { return this.password; }

	public void setPassword(String password) { this.password = password; }

	@Column(name = "privilege")
	public Boolean getPrivilege() { return this.privilege; }

	public void setPrivilege(Boolean privilege) { this.privilege = privilege; }

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "signUp", length = 19)
	public Date getSignUp() { return this.signUp; }

	public void setSignUp(Date signUp) { this.signUp = signUp; }

	@Column(name = "titlesOnly")
	public Boolean getTitlesOnly() { return this.titlesOnly; }

	public void setTitlesOnly(Boolean titlesOnly) { this.titlesOnly = titlesOnly; }

	@Column(name = "subject", length = 100)
	public String getSubject() { return this.subject; }

	public void setSubject(String subject) { this.subject = subject; }
}
