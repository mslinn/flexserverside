package com.mslinn.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "location", catalog = "craigmeister")
public class Location implements Serializable {
	private static final long serialVersionUID = 7435026720682862434L;
	private Integer id;
	private String state;
	private String region;
	private String city;

	public Location() { }

	public Location(String state, String region, String city) {
		this.state = state;
		this.region = region;
		this.city = city;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() { return this.id; }

	public void setId(Integer id) { this.id = id; }

	@Column(name = "state", nullable = false, length = 10)
	public String getState() { return this.state; }

	public void setState(String state) { this.state = state; }

	@Column(name = "region", nullable = false, length = 45)
	public String getRegion() { return this.region; }

	public void setRegion(String region) { this.region = region; }

	@Column(name = "city", nullable = false, length = 45)
	public String getCity() { return this.city; }

	public void setCity(String city) { this.city = city; }
}
