package com.mslinn.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "search", catalog = "craigmeister")
public class Search implements java.io.Serializable {
	private static final long serialVersionUID = 1652462838101649280L;
	private Integer id;
	private Boolean active;
	private String region;
	private String category;
	private String includedKeywords;
	private String excludedKeywords;
	private String userEmail;
	private String minimumValue;
	private String maximumValue;
	private Integer charsShown;
	private Boolean withImages;
	private String otherRecipients;
	private Integer intervalMinutes;
	private Date lastRun;
	private Date nextRun;

	public Search() { }

	public Search(String region, String category, String userEmail, Date nextRun) {
		this.region = region;
		this.category = category;
		this.userEmail = userEmail;
		this.nextRun = nextRun;
	}

	public Search(Boolean active, String region, String category,
			String includedKeywords, String excludedKeywords, String userEmail,
			String minimumValue, String maximumValue, Integer charsShown,
			Boolean withImages, String otherRecipients,
			Integer intervalMinutes, Date lastRun, Date nextRun) {
		this.active = active;
		this.region = region;
		this.category = category;
		this.includedKeywords = includedKeywords;
		this.excludedKeywords = excludedKeywords;
		this.userEmail = userEmail;
		this.minimumValue = minimumValue;
		this.maximumValue = maximumValue;
		this.charsShown = charsShown;
		this.withImages = withImages;
		this.otherRecipients = otherRecipients;
		this.intervalMinutes = intervalMinutes;
		this.lastRun = lastRun;
		this.nextRun = nextRun;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() { return this.id; }

	public void setId(Integer id) { this.id = id; }

	@Column(name = "active")
	public Boolean getActive() { return this.active; }

	public void setActive(Boolean active) { this.active = active; }

	@Column(name = "region", nullable = false, length = 45)
	public String getRegion() { return this.region; }

	public void setRegion(String region) { this.region = region; }

	@Column(name = "category", nullable = false, length = 45)
	public String getCategory() { return this.category; }

	public void setCategory(String category) { this.category = category; }

	@Column(name = "includedKeywords", length = 256)
	public String getIncludedKeywords() { return this.includedKeywords; }

	public void setIncludedKeywords(String includedKeywords) {
		this.includedKeywords = includedKeywords;
	}

	@Column(name = "excludedKeywords", length = 256)
	public String getExcludedKeywords() {
		return this.excludedKeywords;
	}

	public void setExcludedKeywords(String excludedKeywords) {
		this.excludedKeywords = excludedKeywords;
	}

	@Column(name = "userEmail", nullable = false, length = 100)
	public String getUserEmail() { return this.userEmail; }

	public void setUserEmail(String userEmail) { this.userEmail = userEmail; }

	@Column(name = "minimumValue", length = 45)
	public String getMinimumValue() { return this.minimumValue; }

	public void setMinimumValue(String minimumValue) {
		this.minimumValue = minimumValue;
	}

	@Column(name = "maximumValue", length = 45)
	public String getMaximumValue() {
		return this.maximumValue;
	}

	public void setMaximumValue(String maximumValue) {
		this.maximumValue = maximumValue;
	}

	@Column(name = "charsShown")
	public Integer getCharsShown() { return this.charsShown; }

	public void setCharsShown(Integer charsShown) { this.charsShown = charsShown; }

	@Column(name = "withImages")
	public Boolean getWithImages() { return this.withImages; }

	public void setWithImages(Boolean withImages) { this.withImages = withImages; }

	@Column(name = "otherRecipients", length = 256)
	public String getOtherRecipients() { return this.otherRecipients; }

	public void setOtherRecipients(String otherRecipients) {
		this.otherRecipients = otherRecipients;
	}

	@Column(name = "intervalMinutes")
	public Integer getIntervalMinutes() { return this.intervalMinutes; }

	public void setIntervalMinutes(Integer intervalMinutes) {
		this.intervalMinutes = intervalMinutes;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastRun", length = 19)
	public Date getLastRun() { return this.lastRun; }

	public void setLastRun(Date lastRun) { this.lastRun = lastRun; }

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "nextRun", nullable = false, length = 19)
	public Date getNextRun() { return this.nextRun; }

	public void setNextRun(Date nextRun) { this.nextRun = nextRun; }
}
