package com.mslinn.testUtil;

import java.io.File;
import java.io.IOException;

/** Run {@code resetDatabase()} in JUnit's {@code@BeforeClass} method */
public class TestUtil {
	private static final String mysqlHomeLinux   = "/usr/bin/mysql/bin";
	private static final String mysqlHomeMac     = "/usr/local/mysql/bin";
	private static final String mysqlHomeWindows = "%programfiles%/MySQL/MySQL Server 5.0/bin";
	
	/* command line syntax varies slightly between operating systems */
	private static final String dumpCmdLinux   = "mysqldump -u %1$s -p%2$s %3$s > %4$s/%5$s";
	private static final String dumpCmdMac     = "mysqldump -u %1$s -p%2$s %3$s > %4$s/%5$s";
	private static final String dumpCmdWindows = "mysqldump -u %1$s -p%2$s %3$s > %4$s/%5$s";
	
	private static final String restoreCmdLinux   = "mysql -u %1$s -p%2$s %3$s < %4$s/%5$s";
	private static final String restoreCmdMac     = "mysql -u %1$s -p%2$s %3$s < %4$s/%5$s";
	private static final String restoreCmdWindows = "mysql -u %1$s -p%2$s %3$s < %4$s/%5$s";
	
	private File directory = new File(".");
	private String database;
	private String dumpCmd;
	private String password;
	private String restoreCmd;
	private String scriptName = "snapshot.sql";
	private String uname;
	
	public TestUtil(final String database, final String uname, final String password) {
		createTestUtil(database, uname, password, scriptName);
	}

	public TestUtil(final String database, final String uname, final String password, final String scriptName) {
		createTestUtil(database, uname, password, scriptName);
	}

	private void createTestUtil(final String database, final String uname, final String password, final String scriptName) {
		this.database = database;
		this.uname    = uname;
		this.password = password;
		
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.startsWith("linux")) {
			dumpCmd    = mysqlHomeLinux + "/" + dumpCmdLinux;
			restoreCmd = mysqlHomeLinux + "/" + restoreCmdLinux;
		} else if (osName.startsWith("mac")) {
			dumpCmd    = mysqlHomeMac + "/" + dumpCmdMac;
			restoreCmd = mysqlHomeMac + "/" + restoreCmdMac;
		} else if (osName.startsWith("windows")) {
			dumpCmd    = mysqlHomeWindows + "/" + dumpCmdWindows;
			restoreCmd = mysqlHomeWindows + "/" + restoreCmdWindows;
		}
	}
	
	/** @return completed Process */
	public Process dumpDatabase() throws IOException, InterruptedException {
	    return runCmd(dumpCmd);
	}

	/** @return completed Process */
	public Process resetDatabase() throws IOException, InterruptedException {
	    return runCmd(restoreCmd);
	}

	/** @return completed Process */
	private Process runCmd(final String rawCmd) throws IOException, InterruptedException {
		String cmd = String.format(rawCmd, uname, password, database, scriptName);
		ProcessBuilder pb = new ProcessBuilder(cmd).directory(directory);
		Process process = pb.start();
		process.waitFor();
		return process;
	}

	public TestUtil setDirectory(final String directory) { 
		this.directory = new File(directory);
		return this;
	}
}
