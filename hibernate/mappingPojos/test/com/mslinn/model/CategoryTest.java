package com.mslinn.model;

import static org.hibernate.cfg.Environment.FORMAT_SQL;
import static org.hibernate.cfg.Environment.GENERATE_STATISTICS;
import static org.hibernate.cfg.Environment.SHOW_SQL;
import static org.hibernate.cfg.Environment.USE_SQL_COMMENTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.PropertyValueException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.mslinn.testUtil.TestUtil;


public class CategoryTest {
  private static Logger rootLogger = Logger.getRootLogger();

 /** switches between verbose output and just error messages */
  private static boolean debugProblem = false; 
  private static final AnnotationConfiguration annotationConfiguration = 
    new AnnotationConfiguration()
      .addPackage("com.mslinn.model")
      .addAnnotatedClass(Category.class);

  private static Session session;

  private Category category;

  @BeforeClass
  /** Assumes that a database has been backed up and stored as {@code ~/snapshot.sql} */
  public static void classSetup() throws IOException, InterruptedException {
	TestUtil testUtil = new TestUtil("craigmeister", "dude", "craigmeister");
	testUtil.resetDatabase(); // run tests against the same database contents each time
    
	if (debugProblem) {
      annotationConfiguration.setProperty(SHOW_SQL, "true") // log generated SQL to console
        .setProperty(FORMAT_SQL, "true")           // companion to SHOW_SQL
        .setProperty(USE_SQL_COMMENTS, "true")     // companion to SHOW_SQL
        .setProperty(GENERATE_STATISTICS, "true"); // insight is always good
      rootLogger.setLevel(Level.INFO);
    } else
      rootLogger.setLevel(Level.ERROR);

    SessionFactory sessionFactory = 
      annotationConfiguration.configure().buildSessionFactory();

    /** TODO need to obtain a list of all classes marked with @Entity in the 
     * persistable package(s)
     * @see http://www.javaworld.com/javaworld/javatips/jw-javatip113.html?page=2 */

    try {
      session = sessionFactory.openSession();
    } catch (Throwable ex) {
      fail(ex.toString());
    }
  }

  @AfterClass
  public static void classTeardown() {
      session.close();
  }

  @Test
  public void testCreate1() {
      category = (Category)session.get(Category.class, 0L);
      if (category != null)
          fail("There should be no category with id 0");
  }

  @Test
  public void testCreate2() {
    boolean caughtException = false;
    category = new Category();
    category.setFirst("one");
    try {
      session.save(category);
    } catch (PropertyValueException ex) {
      // category.secondary should be null,
      // and that is not allowed because of @NotNull
      caughtException = true;
      // Clear error state or session.save() will fail 
      session.clear(); 
    } finally {
      if (!caughtException)
        fail("Null Category.secondary did not throw a " +
             "PropertyValueException exception");
    }

    // category.id should be assigned by the database
    category.setFirst("one");
    category.setSecond("two");
    Long categoryId = (Long)session.save(category);
    assertNotNull(categoryId);
    session.flush(); // force the SQL INSERT
  }

  // Hibernate doesn�t support generics
  @SuppressWarnings("rawtypes")
  @Test
  public void testCreate3() {
      List cats = session.createQuery("from Category as category").list();
    assertTrue("No Category was saved", cats.size() > 0);
    for (Object cat : cats) {
      category = (Category)cat;
      rootLogger.info("id: " + category.getId() + ", primary=" + 
                      category.getFirst() + ", secondary=" + 
                      category.getSecond());
    }
  
    category.setFirst("asdf");
    category.setSecond("kdkdk");
    session.refresh(category); // wipe out changes to category
    assertNotNull("Category id is null after flush", category.getId());
    assertTrue("Invalid Category id after flush", category.getId() > 0);
    assertEquals(category.getFirst(), "one");
    assertEquals(category.getSecond(), "two");
  }
}
