CREATE DATABASE IF NOT EXISTS craigmeister;
USE craigmeister;

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` SERIAL,
  `first` varchar(45) NOT NULL,
  `second` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
);

DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id` SERIAL,
  `state` varchar(10) NOT NULL,
  `region` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
);

DROP TABLE IF EXISTS `search`;
CREATE TABLE `search` (
  `id` SERIAL,
  `active` tinyint(1) default '1',
  `region` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `includedKeywords` varchar(256) default '',
  `excludedKeywords` varchar(256) default '',
  `userz_id` int(10) unsigned NOT NULL,
  `minimumValue` varchar(45) default NULL,
  `maximumValue` varchar(45) default NULL,
  `charsShown` int(11) default '250',
  `withImages` tinyint(1) default '0',
  `otherRecipients` varchar(256) default '',
  `intervalMinutes` int(11) default '60',
  `lastRun` datetime default '0000-00-00 00:00:00',
  `nextRun` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
);

DROP TABLE IF EXISTS `userz`;
CREATE TABLE `userz` (
  `id` SERIAL,
  `active` tinyint(1) default '1',
  `autoLogin` tinyint(1) default '1',
  `email` varchar(100) NOT NULL,
  `emailOk` tinyint(1) default '0',
  `lastLogin` datetime default '0000-00-00 00:00:00',
  `password` varchar(45) NOT NULL,
  `privilege` tinyint(1) default '0',
  `signUp` datetime default NULL,
  `titlesOnly` tinyint(1) default '0',
  `subject` varchar(100) default 'New Craigslist Alert',
  PRIMARY KEY  (`id`)
);
